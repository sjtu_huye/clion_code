#ifndef __TIEDL_ALG_NET_GEN_H__
#define __TIEDL_ALG_NET_GEN_H__

#ifdef __cpluscplus
extern "C" {
#endif

#include "stdio.h"
#include "itidl_ti.h"

#define LOG(log_level, fmt, ...) printf("<%d> %s %d: ", log_level, __FUNCTION__, __LINE__); printf(fmt, ##__VA_ARGS__);
#define LOG_ERROR(fmt, ...)  LOG(2, fmt, ##__VA_ARGS__); printf("\n");
#define LOG_INFO(fmt, ...)  LOG(0, fmt, ##__VA_ARGS__); printf("\n");


#define TEST_ID 1

typedef struct {
    int32_t numInCh;
    int32_t numOutCh;
    int32_t kernelW;
    int32_t kernelH;
    int32_t strideW;
    int32_t strideH;
    int32_t dilationW;
    int32_t dilationH;
    int32_t padW;
    int32_t padH;
    float32_tidl weightScale;
    float32_tidl biasScale;
    int32_t weightPos;
    int32_t biasPos;
    void *weightPtr;
    void *biasPtr;

} TIDLSingleOpConvCfg;

typedef struct {
    int32_t weightPos;
    int32_t biasPos;
} TIDLSingleOpPoolingCfg;

typedef union {
    TIDLSingleOpConvCfg convCfg;
    TIDLSingleOpPoolingCfg poolCfg;
} TIDLSingleOpLayerParams;

typedef struct {
    TIDLSingleOpLayerParams layerParams;
    int32_t numInBufs;
    int32_t numOutBufs;
    int32_t coreId;
    int32_t layersGroupId;
    int32_t weightsElementSizeInBits;
    int32_t strideOffsetMethod;
    sTIDL_DataParams_t inData[TIDL_NUM_IN_BUFS];
    sTIDL_DataParams_t outData[TIDL_NUM_OUT_BUFS];
} TIDLSingleOpLayerCfg;

typedef struct {
    int32_t inResizeType;
    int32_t resizeWidth;
    int32_t resizeHeight;
    int32_t inWidth;
    int32_t inHeight;
    int32_t inNumChannels;
    int32_t inPadL;
    int32_t inPadT;
    int32_t inNumBatches;
    int32_t rawDataInElementType;
    int32_t inElementType;
    float32_tidl inTensorScale;

    int32_t outWidth;
    int32_t outHeight;
    int32_t outNumChannels;
    int32_t outPadL;
    int32_t outPadT;
    int32_t outChannelPitch;
    int32_t outNumBatches;
    int32_t outElementType;
    float32_tidl outTensorScale;


} TIDLSingleOpIoCfg;

typedef struct {
    int32_t layerType;
    int32_t layerNum;
    int32_t perfsimPos;
    int32_t netInfoSize;
    TIDLSingleOpIoCfg ioCfg;
    TIDLSingleOpLayerCfg layerCfg;
} TIDLSingleOpUsrCfg;



void *TIDL_GetNetBufferAddr(int32_t size);
void *TIDL_GetIoBufferAddr();
void *TIDL_GetSingleOpUsrCfgAddr();

void *TIDL_GetWeightAddr();
void *TIDL_GetBiasAddr();


void TIDL_SingleOpUsrCfgInit();
void TIDL_SingleIoFileGen();
void TIDL_SingleOpNetFileGen();



#ifdef __cpluscplus
}
#endif

#endif