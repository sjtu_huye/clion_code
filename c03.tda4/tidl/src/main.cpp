#include <stdio.h>
#include <errno.h>
#include <string.h>
#include "params.h"
#include "perfsim.h"
#include "tidl_alg_net_gen.h"

#define SEEK_SET    0
#define SEEK_CUR    1
#define SEEL_END    2

#define MAX_NET_FILESIZE 20000000
#define MAX_NET_NUM 5
#define PROC_ID 2
int8_t g_netInfo[MAX_NET_NUM][MAX_NET_FILESIZE] = {0};

int8_t *get_int8_t_pointer(int8_t arr[], int32_t offset)
{
    return &arr[offset];
}

int GenBinFile(const char *fileName, void *buffer, int size)
{
    FILE *fptr;
    int freadStatus = 0;
    fptr = fopen(fileName, "wb+");
    if (fptr == nullptr) {
        printf("open fail errno = %d reason = %s \n", errno, strerror(errno));
        return -1;
    }
    freadStatus = fwrite((void *)buffer, 1, size, fptr);
    fclose(fptr);
    if (freadStatus == 0) {
        printf("Could Not Write %s\n", fileName);
        return -1;
    }
    printf("Writing file %s ... Done. %d bytes\n", fileName, size);
    return 0;
}


int ReadIoFile(const char *fileName, sTIDL_IOBufDesc_t *ioParams)
{
    FILE *fptr;
    int freadStatus = 0;
    fptr = fopen(fileName, "rb");
    if (fptr == nullptr) {
        printf("open fail errno = %d reason = %s \n", errno, strerror(errno));
        return -1;
    }
    freadStatus = fread((void *)ioParams, 1, sizeof(sTIDL_IOBufDesc_t), fptr);
    fclose(fptr);
    if (freadStatus == 0) {
        printf("Could Not Read %s\n", fileName);
        return -1;
    }
    printf("Reading io file %s ... Done. %d bytes\n", fileName, sizeof(sTIDL_IOBufDesc_t));
    return 0;
}

int GenIoFile(const char *fileName, void *ioParams)
{
    return GenBinFile(fileName, ioParams, sizeof(sTIDL_IOBufDesc_t));
}

int ReadNetFile(const char *fileName, int *size, int netId)
{
    FILE *fptr;
    int capacity = 0;
    void *net_buf = NULL;
    if (netId > MAX_NET_NUM) {
        printf("netId=%d exceed %d\n", netId, MAX_NET_NUM);
        return -1;
    }
    fptr = fopen(fileName, "rb");
    if (fptr == nullptr) {
        printf("open fail errno = %d reason = %s \n", errno, strerror(errno));
        return -1;
    }

    fseek(fptr, 0, SEEL_END);
    capacity = ftell(fptr);
    fseek(fptr, 0, SEEK_SET);

    net_buf = (void *)malloc(capacity);
    if (net_buf) {
        size_t read_count = fread((net_buf, capacity, 1, fptr);
        if (read_count != 1) {
            printf("ERR: Unable to read network file\n");
            fclose(fptr);
            free(net_buf);
            return -1;
        }
    }

    fclose(fptr);
    if (capacity > MAX_NET_FILESIZE) {
        printf("ERR: file size exceed %d(%d)", (int)MAX_NET_FILESIZE, capacity);
        free(net_buf);
        return -1;
    }
    memcpy(g_netInfo[netId], net_buf, capacity);
    free(net_buf);
    printf("Reading net file %s ... Done. %d bytes\n", fileName, capacity);
    *size = capacity;
    return 0;
}


int GetFileInfo(char *netFileName, char *ioFileName, sTIDL_IOBufDesc_t *ioParams, int id, int flag)
{
    int size = 0;
    int ret;
    char netNewFileName[256] = {0};
    ret = ReadIoFile((const char *)ioFileName, ioParams);
    if(ret != 0) {
        return -1;
    }

    ret = ReadNetFile((const char *)netFileName, &size, id);
    if(ret != 0) {
        return -1;
    }

    sTIDL_Network_t *net = (sTIDL_Network_t *)g_netInfo[id];
    sPerfSim_t *perfInfo = (sPerfSim_t *)get_int8_t_pointer((int8_t *)(net), net->dataFlowInfo);
    int wPos = net->TIDLLayers[1].layerParams.convParams.weights;
    int bPos = net->TIDLLayers[1].layerParams.convParams.bias;
    int weightSize = bPos - wPos;
    int biasSize = net->dataFlowInfo - bPos;
    printf("%s: weights-%d, bias-%d, dataFlowInfo-%d, (%d, %d)\n", netFileName, wPos, bPos, net->dataFlowInfo, weightSize, biasSize);

    if (flag == 1) {
        strcpy(netNewFileName, netFileName);
        strcat(netNewFileName, ".new");
        ret = GenBinFile(netNewFileName, (void *)net, size);
        if (ret != 0) {
            printf("Write %s fail!\n", netNewFileName);
            return -1;
        }
    }
    
    if (flag == PROC_ID) {
        strcpy(netNewFileName, netFileName);
        strcat(netNewFileName, ".wight");
        int8_t *weightAddr = (int8_t *)&(((int8_t *)net)[net->TIDLLayers[1].layerParams.convParams.weights]);
        ret = GenBinFile(netNewFileName, (void *)weightAddr, weightSize);
        if (ret != 0) {
            printf("Write %s fail!\n", netNewFileName);
            return -1;
        }

        strcpy(netNewFileName, netFileName);
        strcat(netNewFileName, ".new");
        int8_t *biasAddr = (int8_t *)&(((int8_t *)net)[net->TIDLLayers[1].layerParams.convParams.bias]);
        ret = GenBinFile(netNewFileName, (void *)biasAddr, biasSize);
        if (ret != 0) {
            printf("Write %s fail!\n", netNewFileName);
            return -1;
        }

        memcpy(TIDL_GetWeightAddr(), weightAddr, weightSize);
        memcpy(TIDL_GetBiasAddr(), biasAddr, biasSize);
        printf("weight pos: %d, bias pos: %d\n", net->TIDLLayers[1].layerParams.convParams.weights, net->TIDLLayers[1].layerParams.convParams.bias);
    }
    return 0;
}

int ShowFileInfo() {
    printf("sizeof(sTIDL_Network_t): %d, sizeof(sPerfSim_t): %d\n", sizeof(sTIDL_Network_t), sizeof(sPerfSim_t));
    printf("sizeof(sTIDL_Layer_t): %d\n", sizeof(sTIDL_Layer_t));
    printf("sizeof(sPerfSimConfig_t): %d, sizeof(sDataFlowInfo_t): %d\n", sizeof(sPerfSimConfig_t), sizeof(sDataFlowInfo_t));

    int id = 0;

    sTIDL_IOBufDesc_t ioParams0;
    auto ret = GetFileInfo("single_op_net_conv.bin", "single_op_io_conv.bin", &ioParams0, id, 0);
    if (ret != 0) {
        return -1;
    }
    sTIDL_Network_t *net0 = (sTIDL_Network_t *)g_netInfo[id++];
    sPerfSim_t *perfInfo0 = (sPerfSim_t *)get_int8_t_pointer((int8_t *)(net0), net0->dataFlowInfo);

    sTIDL_IOBufDesc_t ioParams_224;
    auto ret = GetFileInfo("tidl_net_conv_224.bin", "tidl_io_conv_224_1.bin", &ioParams_224, id, id + 1);
    if (ret != 0) {
        return -1;
    }
    sTIDL_Network_t *net_224 = (sTIDL_Network_t *)g_netInfo[id++];
    sPerfSim_t *perfInfo_224 = (sPerfSim_t *)get_int8_t_pointer((int8_t *)(net_224), net_224->dataFlowInfo);

    sTIDL_IOBufDesc_t ioParams_112;
    auto ret = GetFileInfo("tidl_net_conv_112.bin", "tidl_io_conv_112_1.bin", &ioParams_112, id, id + 1);
    if (ret != 0) {
        return -1;
    }
    sTIDL_Network_t *net_112 = (sTIDL_Network_t *)g_netInfo[id++];
    sPerfSim_t *perfInfo_112 = (sPerfSim_t *)get_int8_t_pointer((int8_t *)(net_112), net_112->dataFlowInfo);
}

int SingleOpNetAndIoGen() {
    TIDLSingleOpUsrCfg *usrCfg = (TIDLSingleOpUsrCfg *)TIDL_GetSingleOpUsrCfgAddr();
    TIDL_SingleOpUsrCfgInit();
    TIDL_SingleIoFileGen();
    TIDL_SingleOpNetFileGen();

    void *netBuf = TIDL_GetNetBufferAddr(0);
    void *ioBuf = TIDL_GetIoBufferAddr();
    char *netFileName = "single_op_net_conv.bin";
    char *ioFileName = "single_op_io_conv.bin";
    int netSize = usrCfg->netInfoSize;
    auto ret = GenBinFile(netFileName, netBuf, netSize);
    if(ret != 0) {
        return -1;
    }
    ret = GenIoFile(ioFileName, netBuf);
    if(ret != 0) {
        return -1;
    }
    return 0;
}


int main()
{
    auto ret = ShowFileInfo();
    if (ret != 0) {
        return -1;
    }
    return 0;

    auto ret = SingleOpNetAndIoGen();
    if (ret != 0) {
        return -1;
    }
    return 0;
}