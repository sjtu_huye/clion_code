#include "tidl_alg_net_gen.h"
#include "perfsim.h"
#include <string.h>

TIDLSingleOpUsrCfg g_singleOpUsrCfg = {0};
int8_t g_weight[100000] = {0};
int8_t g_bias[1000] = {0};
int8_t g_net[50000] = {0};
int8_t g_io[sizeof(sTIDL_IOBufDesc_t)] = {0};

#define ALIGN_SIZE(x, y)  ((((x) + ((y) - 1))/ (y)) * (y))

int32_t TIDL_Align64(int32_t value)
{
    int32_t remain = value % 64;
    int32_t factor = (remain == 0) ? 0 : 1;
    return (value + (64 - remain) * factor);
}

void *TIDL_GetNetBufferAddr(int32_t size) {
    return (void *)&g_net[0];
}

void *TIDL_GetIoBufferAddr() {
    return (void *)&g_io[0];
}

void *TIDL_GetWeightAddr() {
    return (void *)&g_weight[0];
}


void *TIDL_GetBiasAddr() {
    return (void *)&g_bias[0];
}

void *TIDL_GetSingleOpUsrCfgAddr() {
    return (void *)&g_singleOpUsrCfg;
}


void TIDL_SingleOpUsrCfgInit() {
    TIDLSingleOpUsrCfg *usrCfg = (TIDLSingleOpUsrCfg *)TIDL_GetSingleOpUsrCfgAddr();
    TIDLSingleOpConvCfg *convCfg = &usrCfg->layerCfg.layerParams.convCfg;
    memset(usrCfg, 0, sizeof(TIDLSingleOpUsrCfg));

    int layerNum = 2;
    int numInCh = 3;
    int numOutCh = 64;
    int kw = 7;
    int kH = 7;

    int32_t netWorkPreSize, weightSize, perfsimSize;
    netWorkPreSize = sizeof(sTIDL_Network_t) - sizeof(sTIDL_Layer_t) * (TIDL_NUM_MAX_LAYERS - layerNum);
    weightSize = numInCh * numOutCh * kW * kH;
    perfsimSize = sizeof(sPerfSimConfig_t) + sizeof(int32_t) * MEMTYPE_TOTAL + sizeof(sDataFlowInfo_t) * layerNum;

    int pos = TIDL_Align64(netWorkPreSize);
    convCfg->weightPos = pos;

    pos += TIDL_Align64(weightSize);
    convCfg->biasPos = pos;

    convCfg->weightPtr = TIDL_GetWeightAddr();
    convCfg->biasPtr = TIDL_GetBiasAddr();

    pos += TIDL_Align64(numOutCh * 2);
    usrCfg->perfsimPos = pos;

    pos += perfsimSize;
    usrCfg->netInfoSize = pos;

}

void TIDL_SingleIoFileGen() {
    TIDLSingleOpUsrCfg *usrCfg = (TIDLSingleOpUsrCfg *)TIDL_GetSingleOpUsrCfgAddr();
    sTIDL_IOBufDesc_t *ioBuf = (sTIDL_IOBufDesc_t *)TIDL_GetIoBufferAddr();
    memset(ioBuf, 0, sizeof(sTIDL_IOBufDesc_t));


}

void TIDL_FillNetWorkInfo(void *netBuf, TIDLSingleOpUsrCfg *usrCfg){

}
void TIDL_FillOpData(void *netBuf, TIDLSingleOpUsrCfg *usrCfg){
    if(usrCfg->layerType == TIDL_ConvolutionLayer) {
        TIDLSingleOpConvCfg *convCfg = &usrCfg->layerCfg.layerParams.convCfg;
        int8_t *weightAddr = (int8_t *)&(((int8_t *)netBuf)[convCfg->weightPos]);
        int8_t *biasAddr = (int8_t *)&(((int8_t *)netBuf)[convCfg->biasPos]);

        int32_t weightSize = convCfg->numOutCh * convCfg->numInCh * convCfg->kernelW * convCfg->kernelH;
        memcpy(weightAddr, convCfg->weightPtr, weightSize);
        memcpy(biasAddr, convCfg->biastPtr, convCfg->numOutCh *2);

    } else {
        LOG_ERROR("not support op-%d!", usrCfg->layerType);
    }
}

void TIDL_PerfsimConfigGen(sPerfSimConfig_t *simConfig, TIDLSingleOpUsrCfg * usrCfg) {

}
void TIDL_PerfMemInfoGen(int32_t *memSize) {

}
void TIDL_PerfDataFlowInfoGen(sPerfSim_t *perfSim, TIDLSingleOpUsrCfg * usrCfg) {

}

void TIDL_FillPerfsimInfo(void *netBuf, TIDLSingleOpUsrCfg *usrCfg)
{
    sPerfSim_t *perfSim = (sPerfSim_t *)&(((int8_t)netBuf)[usrCfg->perfsimPos]);
    TIDL_PerfsimConfigGen(&perfSim->simConfig, usrCfg);
    TIDL_PerfMemInfoGen(&perfSim->memorySizeRequirement[0]);
    TIDL_PerfDataFlowInfoGen(perfSim, usrCfg);
}

void TIDL_SingleOpNetFileGen() {
    TIDLSingleOpUsrCfg *usrCfg = (TIDLSingleOpUsrCfg *)TIDL_GetSingleOpUsrCfgAddr();
    void *netBuf = TIDL_GetNetBufferAddr(usrCfg->netInfoSize);
    memset(netBuf, 0, usrCfg->netInfoSize);

    if(usrCfg->layerNum > 2) {
        LOG_INFO("net layer num exceed 2!")
        return;
    }

    TIDL_FillNetWorkInfo(netBuf, usrCfg);
    TIDL_FillOpData(netBuf, usrCfg);
    TIDL_FillPerfsimInfo(netBuf, usrCfg);
}