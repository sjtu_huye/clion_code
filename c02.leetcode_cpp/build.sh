#!/bin/bash
set -e

# default x86
TARGET_OS=x86
TBUILD_VERSION=Release
CLEAN_FLAG=OFF
function parse_args()
{
    for opt in "$@"
    do
        case "${opt}" in
            clean)
                CLEAN_FLAG=ON
                ;;
            x86)
                TARGET_OS=x86
                ;;
            arm)
                TARGET_OS=ARM
                PACKAGE_SUFFIX=arm
                ;;
            debug)
                TBUILD_VERSION=Debug
                ;;
        esac
    done
}

export HOME_DIR=$(dirname $(readlink -f $0))
BIN_DIR=${HOME_DIR}/bin
LIB_DIR=${HOME_DIR}/lib
BUILD_DIR=${HOME_DIR}/build

parse_args "$@"

if [[ "${TBUILD_VERSION}" == "Release" ]] ; then
    BUILD_DIR="${BUILD_DIR}/release"
else
    BUILD_DIR="${BUILD_DIR}/debug"
fi

if [[ "${CLEAN_FLAG}" == "ON" ]] ; then
    rm -rf ${BUILD_DIR}
    rm -rf ${LIB_DIR}
fi

COMM_ARGS="-DTARGET_OS=${TARGET_OS}"
COMM_ARGS="${COMM_ARGS} -DCMAKE_BUILD_TYPE=${TBUILD_VERSION}"

echo "${COMM_ARGS}"
mkdir -p ${BUILD_DIR}
cd ${BUILD_DIR}
echo "Building...."
cmake ${COMM_ARGS} ${HOME_DIR}
make