#ifndef L_CONTAINER_WITH_MOST_WATER__
#define L_CONTAINER_WITH_MOST_WATER__

#include <vector>
using namespace std;

namespace LeetCode {
namespace Leet0011 {
class Solution {
public:
    Solution();
    virtual ~Solution() = default;
    int maxArea(vector<int>& height);
};
}
}

#endif // L_CONTAINER_WITH_MOST_WATER__
