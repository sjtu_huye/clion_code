#ifndef L_NUMBER_OF_ISLANDS__
#define L_NUMBER_OF_ISLANDS__

#include "util.h"
#include <vector>
using namespace std;

namespace LeetCode {
namespace Leet0200 {
class Solution {
public:
    Solution();
    virtual ~Solution() = default;
    int numIslands(vector<vector<char>>& grid);
};
}
}

#endif // L_NUMBER_OF_ISLANDS__

