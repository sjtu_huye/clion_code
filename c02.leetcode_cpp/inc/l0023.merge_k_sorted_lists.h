#ifndef L_MERGE_K_SORTED_LISTS__
#define L_MERGE_K_SORTED_LISTS__

#include "util.h"
#include <vector>
using namespace std;

namespace LeetCode {
namespace Leet0023 {
class Solution {
public:
    Solution();
    virtual ~Solution() = default;
    ListNode* mergeKLists(vector<ListNode*>& lists);
};
}
}

#endif // L_MERGE_K_SORTED_LISTS__
