#ifndef L_REVERSE_LINKED_LIST_II__
#define L_REVERSE_LINKED_LIST_II__

#include "util.h"
using namespace std;

namespace LeetCode {
namespace Leet0092 {
class Solution {
public:
    Solution();
    virtual ~Solution() = default;
    ListNode* reverseBetween(ListNode* head, int left, int right);
};
}
}

#endif // L_REVERSE_LINKED_LIST_II__
