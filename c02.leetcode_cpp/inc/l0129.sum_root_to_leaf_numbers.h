#ifndef L_SUM_ROOT_TO_LEAF_NUMBERS__
#define L_SUM_ROOT_TO_LEAF_NUMBERS__

#include "util.h"
using namespace std;

namespace LeetCode {
namespace Leet0129 {
class Solution {
public:
    Solution();
    virtual ~Solution() = default;
    int sumNumbers(TreeNode* root);
};
}
}

#endif // L_SUM_ROOT_TO_LEAF_NUMBERS__
