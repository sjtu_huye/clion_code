#ifndef L_INTERSECTION_OF_TWO_LINKED_LISTS__
#define L_INTERSECTION_OF_TWO_LINKED_LISTS__

#include "util.h"
using namespace std;

namespace LeetCode {
namespace Leet0160 {
class Solution {
public:
    Solution();
    virtual ~Solution() = default;
    ListNode *getIntersectionNode(ListNode *headA, ListNode *headB);
};
}
}

#endif // L_INTERSECTION_OF_TWO_LINKED_LISTS__
