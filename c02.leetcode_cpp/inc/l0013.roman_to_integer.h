#ifndef L_ROMAN_TO_INT_H__
#define L_ROMAN_TO_INT_H__

#include <string>
using namespace std;

namespace LeetCode {
namespace Leet0013 {
class Solution {
public:
    Solution();
    virtual ~Solution() = default;
    int romanToInt(string s);
};
}
}

#endif // L_ROMAN_TO_INT_H__
