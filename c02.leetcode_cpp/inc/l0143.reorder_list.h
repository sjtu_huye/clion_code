#ifndef L_REORDER_LIST__
#define L_REORDER_LIST__

#include "util.h"
using namespace std;

namespace LeetCode {
namespace Leet0143 {
class Solution {
public:
    Solution();
    virtual ~Solution() = default;
    void reorderList(ListNode* head);
};
}
}

#endif // L_REORDER_LIST__
