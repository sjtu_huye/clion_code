#ifndef L_MAX_AREA_OF_ISLAND__
#define L_MAX_AREA_OF_ISLAND__

#include "util.h"
#include <vector>
using namespace std;

namespace LeetCode {
namespace Leet0695 {
class Solution {
public:
    Solution();
    virtual ~Solution() = default;
    int maxAreaOfIsland(vector<vector<int>>& grid);
};
}
}

#endif // L_MAX_AREA_OF_ISLAND__
