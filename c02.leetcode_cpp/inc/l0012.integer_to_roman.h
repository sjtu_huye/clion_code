#ifndef L_INTEGER_TO_ROMAN_H__
#define L_INTEGER_TO_ROMAN_H__

#include <string>
using namespace std;

namespace LeetCode {
namespace Leet0012 {
class Solution {
public:
    Solution();
    virtual ~Solution() = default;
    string intToRoman(int num);
};
}
}

#endif // L_INTEGER_TO_ROMAN_H__
