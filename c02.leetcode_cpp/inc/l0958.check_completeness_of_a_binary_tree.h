#ifndef L_CHECK_COMPLETENESS_OF_A_BINARY_TREE__
#define L_CHECK_COMPLETENESS_OF_A_BINARY_TREE__

#include "util.h"
using namespace std;

namespace LeetCode {
namespace Leet0958 {
class Solution {
public:
    Solution();
    virtual ~Solution() = default;
    bool isCompleteTree(TreeNode* root);
};
}
}

#endif // L_CHECK_COMPLETENESS_OF_A_BINARY_TREE__
