#include "l0013.roman_to_integer.h"
#include <string>
#include <unordered_map>

namespace LeetCode {
namespace Leet0013 {
Solution::Solution() {}
/*
罗马数字包含以下七种字符： I， V， X， L，C，D 和 M。
字符          数值
I             1
V             5
X             10
L             50
C             100
D             500
M             1000
例如， 罗马数字 2 写做 II ，即为两个并列的 1。12 写做 XII ，即为 X + II 。 27 写做  XXVII, 即为 XX + V + II 。

链接：https://leetcode.cn/problems/roman-to-integer/
*/
int Solution::romanToInt(string s)
{
    const std::unordered_map<string, int> mapRoman = {
        {"M",  1000},
        {"CM", 900},
        {"D",  500},
        {"CD", 400},
        {"C",  100},
        {"XC", 90},
        {"L",  50},
        {"XL", 40},
        {"X",  10},
        {"IX", 9},
        {"V",  5},
        {"IV", 4},
        {"I",  1},
    };
    // 按照两位一查找，如果不存在则退一位查找
    int size = s.length();
    int ans = 0;
    string tmp;
    for (size_t i = 0; i < size; i++) {
        if (i == size - 1) {
            tmp = s[i];
        } else {
            tmp = s.substr(i, 2);
        }
        
        if (mapRoman.find(tmp) != mapRoman.end()) {
            ans += mapRoman.at(tmp);
            i++;
        } else {
            ans += mapRoman.at(s.substr(i, 1));
        }
    }
    return ans;

}
}
}