#include "l0012.integer_to_roman.h"
#include <string>

namespace LeetCode {
namespace Leet0012 {
Solution::Solution() {}
/*
罗马数字包含以下七种字符： I， V， X， L，C，D 和 M。
字符          数值
I             1
V             5
X             10
L             50
C             100
D             500
M             1000
例如， 罗马数字 2 写做 II ，即为两个并列的 1。12 写做 XII ，即为 X + II 。 27 写做  XXVII, 即为 XX + V + II 。

链接：https://leetcode.cn/problems/integer-to-roman
*/
string Solution::intToRoman(int num)
{
    const std::pair<int, string> mapRoman[] = {
        {1000, "M"},
        {900,  "CM"},
        {500,  "D"},
        {400,  "CD"},
        {100,  "C"},
        {90,   "XC"},
        {50,   "L"},
        {40,   "XL"},
        {10,   "X"},
        {9,    "IX"},
        {5,    "V"},
        {4,    "IV"},
        {1,    "I"},
    };
    std::string roman;
    for (const auto &[value, symbol] : mapRoman) {  // C++17才支持
        while (num >= value) {
            num -= value;
            roman += symbol;
        }
        if (num == 0) {
            break;
        }
    }
    return roman;
}
}
}