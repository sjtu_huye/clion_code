#include <algorithm>
#include <vector>
#include <string>
#include <tuple>
#include <functional>
#include <iostream>

class table_generator
{
    using table_t = std::vector<std::tuple<std::string, std::size_t>>;

private:
    auto const & add_word()
    {
        table_.reserve(table_.size() + 1);
        table_.emplace_back(word_, hash_word(word_));
        return table_.back();
    }

    bool next_word()
    {
        return std::next_permutation(word_.begin(), word_.end());
    }

    static std::size_t hash_word(std::string const & word)
    {
        return std::hash<std::string>{}(word);
    }

public:
    table_generator(std::string alphabet)
        : word_{alphabet}
    {}

    bool operator()(std::string & word, std::size_t &hash)  // hash 传出引用
    {
        if (table_.empty()) {
            std::sort(word_.begin(), word_.end()); // 首次需要打印
        } else {
            if (!next_word())
                return false;
        }

        std::tie(word, hash) = add_word();
        return true;
    }

private:
    table_t table_;
    std::string word_; // 不应该是引用
};

int main()
{
    table_generator proble4Input("jqinv");

    std::string word;
    std::size_t hash;
    int cont = 0;
    while (proble4Input(word, hash)) {
        std::cout << word << " " << std::hex << hash << "\n";
        cont++;
    }
    std::cout <<cont << "\n";
}
