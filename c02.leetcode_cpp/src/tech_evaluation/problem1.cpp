#include <iostream>
#include <set>
#include <math.h>

using namespace std;

void GetUniqueSubStringNum(string &str) {
    int size = str.size();
    set<string> counter;
    for (int i = 0; i < str.size() - 2; i++) {
        counter.insert(str.substr(i, 3));
    }
    cout << counter.size() << endl;
}

void PrintStr(string &str) {
    int size = str.size();
    // calc layer num 2^n  , 1, (1+3)=4, (1+3+5)=9, 16
    int layer = sqrt(size);
    if (layer * layer < size) {
        layer++;
    }
    int cont = 0;
    int layerCNum = 1;
    for (int i = 0; i < layer; i++) {
        int spaceNum = layer - i - 1;
        for (int j = 0; j < spaceNum; j++) {
            std::cout<<" ";
        }
        for (int j = 0; j < layerCNum; j++) {
            std::cout<<str[cont++];
            if(cont >= size) {
                break;
            }
        }
        std::cout<<"\n";
        layerCNum += 2;
    }
}

int main()
{
    string str = "asdfasdfasdfafafasdsdfsadfasdfgfafasdffasdfasdfafasasfedfafafasdfasd";
    set<char> counter;

    for (int i=0; i < str.size(); i++)
    {
        counter.insert(str[i]);
    }
    cout << counter.size() << endl;
    GetUniqueSubStringNum(str);
    PrintStr(str);
    return 0;
}