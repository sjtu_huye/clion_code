#include <iostream>
#include <set>
#include <vector>
#include<time.h>

using namespace std;

int direcInfo[][2] = {{0, 1}, {1, 0}, {0, -1}, {-1, 0}};
long long g_cont = 0;

void PrintfGrid(vector<vector<int>> &grid) {
    std::cout<< "********************\n";
    for(auto &y : grid) {
        for(auto &x : y) {
            std::cout<< x;
        }
        std::cout<< "\n";
    }
    std::cout<< "********************\n";
}

// dfs + 回溯即可完成路径全遍历
void dfs(vector<vector<int>> &grid, int x, int y, int num, int N) {
    // dfs, 每次标记走过的路径，需要回溯清除标记
    if (x < 0 || x >= N || y < 0 || y >= N) {
        return;
    }
    // 校验
    if (grid[y][x] == 1) {
        return;
    }

    if (num == 0) {
        g_cont++;
        // 维测
        // grid[y][x] = 1;
        // PrintfGrid(grid);
        // grid[y][x] = 0;
        return;
    }
    grid[y][x] = 1; // 回溯
    for (int i = 0; i < 4; i++) {
        dfs(grid, x + direcInfo[i][1], y + direcInfo[i][0], num - 1, N);
    }
    grid[y][x] = 0; // 回溯
}

long long solveProblem2 (int N)
{
    // dfs, 每次标记走过的路径，需要回溯清除标记
    g_cont = 0;
    vector<vector<int>> grid(N, vector<int> (N, 0));
    grid[0][0] = 1;
    dfs(grid, 1, 0, N - 1, N + 1);
    return g_cont;

    return 0;
}

int main ()
{
    double time_Start = (double)clock(); //开始时间
    for (int i = 3; i <= 25; i++) {
        cout << solveProblem2(i) << endl;
    }
    double time_Finish = (double)clock(); //结束时间
    printf("operate time: %.2fms",(time_Finish - time_Start)); //输出
    return 0;
}
