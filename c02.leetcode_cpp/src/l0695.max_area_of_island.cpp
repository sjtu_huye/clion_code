#include "l0695.max_area_of_island.h"
#include <numeric>
#include <string>
#include <queue>

namespace LeetCode {
namespace Leet0695 {
Solution::Solution() {}
/*
给你一个大小为 m x n 的二进制矩阵 grid 。
岛屿 是由一些相邻的 1 (代表土地) 构成的组合，这里的「相邻」要求两个 1 必须在 水平或者竖直的四个方向上 相邻。你可以假设 grid 的四个边缘都被 0（代表水）包围着。
岛屿的面积是岛上值为 1 的单元格的数目。

计算并返回 grid 中最大的岛屿面积。如果没有岛屿，则返回面积为 0 。
输入：grid = {
    {0,0,1,0,0,0,0,1,0,0,0,0,0},
    {0,0,0,0,0,0,0,1,1,1,0,0,0},
    {0,1,1,0,1,0,0,0,0,0,0,0,0},
    {0,1,0,0,1,1,0,0,1,0,1,0,0},
    {0,1,0,0,1,1,0,0,1,1,1,0,0},
    {0,0,0,0,0,0,0,0,0,0,1,0,0},
    {0,0,0,0,0,0,0,1,1,1,0,0,0},
    {0,0,0,0,0,0,0,1,1,0,0,0,0}}
输出：6
解释：答案不应该是 11 ，因为岛屿只能包含水平或垂直这四个方向上的 1。

输入：grid = {{0,0,0,0,0,0,0,0}}
输出：0

链接：https://leetcode.cn/problems/max-area-of-island
*/

int DFS(vector<vector<int>>& grid, int x, int y) {
    int out = 0;
    int direct[][2] = {{0, -1}, {1, 0}, {0, 1}, {-1, 0}};
    int rNum = grid.size();
    int cNum = grid[0].size();
    if (x < 0 || (x > rNum - 1) || y < 0 || (y > cNum - 1) || grid[x][y] == 0) {
        return out;
    }
    out = 1;
    grid[x][y] = 0;
    
    for(int i = 0; i < 4; i++) {
        out += DFS(grid, x + direct[i][0], y + direct[i][1]);
    }
    return out;
}

int Solution::maxAreaOfIsland(vector<vector<int>>& grid) {
    int rNum = grid.size();
    int cNum = grid[0].size();
    int ans = 0;
    for(int i = 0; i < rNum; i++) {
        for(int j = 0; j< cNum; j++) {
            if(grid[i][j] != 0) {
                ans = max(ans, DFS(grid, i, j));
            }
        }
    }
    return ans;
}

}
}