#include "l0958.check_completeness_of_a_binary_tree.h"
#include <string>
#include <queue>

namespace LeetCode {
namespace Leet0958 {
Solution::Solution() {}
/*
给定一个二叉树的 root ，确定它是否是一个 完全二叉树 。

在一个 完全二叉树 中，除了最后一个关卡外，所有关卡都是完全被填满的，并且最后一个关卡中的所有节点都是尽可能靠左的。它可以包含 1 到 2h 节点之间的最后一级 h 。

输入：root = [1,2,3,4,5,6]
输出：true
解释：最后一层前的每一层都是满的（即，结点值为 {1} 和 {2,3} 的两层），且最后一层中的所有结点（{4,5,6}）都尽可能地向左

输入：root = [1,2,3,4,5,null,7]
输出：false
解释：值为 7 的结点没有尽可能靠向左侧。

链接：https://leetcode.cn/problems/check-completeness-of-a-binary-tree
*/

bool Solution::isCompleteTree(TreeNode* root) {
    queue<TreeNode*> q;
    q.push(root);
    int flag = 0;
    while(!q.empty()) {
        TreeNode* front = q.front();
        // 本题是所有点都靠左，所以只要出现right为空，则不应该有后继节点
        // std::cout<< front->val <<std::endl;
        if(front->left != nullptr && front->right != nullptr) {
            if(flag == 1) {
                return false;
            }
            q.push(front->left);
            q.push(front->right);
            q.pop();
        } else if(front->left != nullptr && front->right == nullptr) {
            if(front->left->left!= nullptr || front->left->right!= nullptr || flag == 1) {
                return false;
            }
            q.push(front->left);
            flag = 1;  // 出现一次后就不存在后续还有叶子节点的父节点
            q.pop();
        } else if(front->left == nullptr && front->right == nullptr) {
            q.pop();
            flag = 1;  // 出现一次后就不存在后续还有叶子节点的父节点
        } else {
            return false;
        }
    }
    return true;
}
}
}