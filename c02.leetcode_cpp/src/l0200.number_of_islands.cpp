#include "l0200.number_of_islands.h"
#include <numeric>
#include <string>
#include <queue>

namespace LeetCode {
namespace Leet0200 {
Solution::Solution() {}
/*
给你一个由 '1'（陆地）和 '0'（水）组成的的二维网格，请你计算网格中岛屿的数量
输入：grid = [
  ["1","1","1","1","0"],
  ["1","1","0","1","0"],
  ["1","1","0","0","0"],
  ["0","0","0","0","0"]
]
输出：1

输入：grid = [
  ["1","1","0","0","0"],
  ["1","1","0","0","0"],
  ["0","0","1","0","0"],
  ["0","0","0","1","1"]
]
输出：3

链接：https://leetcode.cn/problems/number-of-islands
*/

int DFS(vector<vector<char>>& grid, int x, int y) {
    int out = 0;
    int direct[][2] = {{0, -1}, {1, 0}, {0, 1}, {-1, 0}};
    int rNum = grid.size();
    int cNum = grid[0].size();
    if (x < 0 || (x > rNum - 1) || y < 0 || (y > cNum - 1) || grid[x][y] == '0') {
        return out;
    }
    out = 1;
    grid[x][y] = '0';
    
    for(int i = 0; i < 4; i++) {
        out += DFS(grid, x + direct[i][0], y + direct[i][1]);
    }
    return out;
}

int Solution::numIslands(vector<vector<char>>& grid) {
    int rNum = grid.size();
    int cNum = grid[0].size();
    int ans = 0;
    for(int i = 0; i < rNum; i++) {
        for(int j = 0; j< cNum; j++) {
            if(grid[i][j] != '0') {
                DFS(grid, i, j);
                ans++;
            }
        }
    }
    return ans;
}

}
}