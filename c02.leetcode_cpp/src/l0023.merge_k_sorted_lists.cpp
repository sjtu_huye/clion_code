#include "l0023.merge_k_sorted_lists.h"
#include <string>

namespace LeetCode {
namespace Leet0023 {
Solution::Solution() {}
/*
请你将所有链表合并到一个升序链表中，返回合并后的链表。
输入：lists = [[1,4,5],[1,3,4],[2,6]]
输出：[1,1,2,3,4,4,5,6]
解释：链表数组如下：
[
  1->4->5,
  1->3->4,
  2->6
]
将它们合并到一个有序链表中得到。
1->1->2->3->4->4->5->6

输入：lists = []
输出：[]

输入：lists = [[]]
输出：[]

链接：https://leetcode.cn/problems/merge-k-sorted-lists/
*/

ListNode* Solution::mergeKLists(vector<ListNode*>& lists) {
    int n = lists.size();
    ListNode* out = new ListNode(-1);
    ListNode* head = out;
    while(1) {
        int min = 10001;
        int idx = -1;
        for(int i = 0; i < n; i++) {
            if (lists[i]) {
                if(lists[i]->val < min) {
                    min = lists[i]->val;
                    idx = i;
                }
            }
        }
        if (idx == -1) {
            break;
        }
        head->next = lists[idx];
        head = head->next;
        lists[idx] = lists[idx]->next;
    }
    head = out->next;
    delete out;
    return head;
}
}
}
