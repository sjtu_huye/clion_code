#include "l0143.reorder_list.h"
#include <string>

namespace LeetCode {
namespace Leet0143 {
Solution::Solution() {}
/*
给定一个单链表 L 的头节点 head ，单链表 L 表示为：

L0 → L1 → … → Ln - 1 → Ln
请将其重新排列后变为：

L0 → Ln → L1 → Ln - 1 → L2 → Ln - 2 → …
不能只是单纯的改变节点内部的值，而是需要实际的进行节点交换。

输入：head = [1,2,3,4]
输出：[1,4,2,3]

输入：head = [1,2,3,4,5]
输出：[1,5,2,4,3]

链接：https://leetcode.cn/problems/reorder-list
*/

int getListSize(ListNode* head) 
{
    ListNode *p = head;
    int cont = 0;
    while(p) {
        p = p->next;
        cont++;
    }
    return cont;
}

ListNode* reverseList(ListNode* head)
{
    if(head == nullptr) return head;
    ListNode* p = head;
    ListNode* pre = nullptr;
    ListNode* tmp;
    while(p) {
        tmp = p->next;
        p->next = pre;
        pre = p;
        p = tmp;
    }
    return pre;
}

void Solution::reorderList(ListNode* head) {
    // l0, l1, ... l((n-1)/2)
    // ln, ln-1, ... l((n+1)/2)
    int n = getListSize(head);
    int pos = (n+1)/2;
    int cont = 0;
    ListNode *p2 = head;
    ListNode *preP2 = nullptr;
    // std::cout << n << std::endl;
    while(cont < pos) {
        preP2 = p2;
        p2 = p2->next;
        cont++;
    }
    if(preP2) preP2->next = nullptr;  // 将两个链的未被都置空，避免后续处理成环
    // std::cout << p2->val << std::endl;
    // 查找至ln，然后对ln后续翻转
    p2 = reverseList(p2);
    // std::cout << p2->val << std::endl;
    // 交叉赋值
    ListNode* out = head;
    ListNode* tmp;
    for(int i = 0; i < pos; i++) {
        if(p2 == nullptr) break;  // 奇数情况后级链表会少一个，因此会存在空指针
        tmp = out->next;
        out->next = p2;
        out = tmp;
        tmp = p2->next;
        p2->next = out;
        p2 = tmp;
    }
}
}
}
