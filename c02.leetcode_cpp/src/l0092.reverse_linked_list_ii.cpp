#include "l0092.reverse_linked_list_ii.h"
#include <string>

namespace LeetCode {
namespace Leet0092 {
Solution::Solution() {}
/*
给你单链表的头指针 head 和两个整数 left 和 right ，其中 left <= right 。请你反转从位置 left 到位置 right 的链表节点，返回 反转后的链表 。

输入：head = [1,2,3,4,5], left = 2, right = 4
输出：[1,4,3,2,5]

输入：head = [5], left = 1, right = 1
输出：[5]

链接：https://leetcode.cn/problems/reverse-linked-list-ii
*/

ListNode* Solution::reverseBetween(ListNode* head, int left, int right) {
    // 记录一个pre，然后从l开始反转链表，到r后记录个tail
    ListNode* proc = head;
    ListNode* preHead = nullptr;
    ListNode* leftNode = nullptr;
    ListNode* rightNode = nullptr;
    ListNode* pre = nullptr;
    ListNode* tail = nullptr;
    ListNode* tmp;
    int cont = 0;
    while(cont < right) {
        if(cont < left - 2) {
            proc = proc->next;
        }
        if(cont == left - 2) {
            preHead = proc;
            pre = proc;
            proc = proc->next;
        }
        if(cont == left - 1) {
            leftNode = proc;     
        }
        if(cont >= left - 1 && cont < right) {
            tmp = proc->next;
            proc->next = pre;
            pre = proc;
            proc = tmp;
        }
        if(cont >= right - 1) {
            rightNode = pre;
            tail = proc;
        }
        cont++;
    }
    // 头尾转换
    if(preHead != nullptr) {
        preHead->next = rightNode;
    } else {
        head = rightNode;
    }
    leftNode->next = tail;

    return head;
}
}
}
