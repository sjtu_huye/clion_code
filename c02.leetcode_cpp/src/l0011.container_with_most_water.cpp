#include "l0011.container_with_most_water.h"

namespace LeetCode {
namespace Leet0011 {
Solution::Solution() {}
/*
给定一个长度为 n 的整数数组 height 。有 n 条垂线，第 i 条线的两个端点是 (i, 0) 和 (i, height[i]) 。
找出其中的两条线，使得它们与 x 轴共同构成的容器可以容纳最多的水。
返回容器可以储存的最大水量。

链接：https://leetcode.cn/problems/container-with-most-water
*/
int Solution::maxArea(vector<int>& height)
{
    // 双指针，每次选择更大的
    int l = 0;
    int r = height.size() - 1;
    int maxValue = 0;
    while (l < r) {
        int value = std::min(height[l], height[r]) * (r - l);
        maxValue = std::max(maxValue, value);
        if (height[l] < height[r])
            l++;
        else
            r--;
    }
    
    return maxValue;
}
}
}