
#include <iostream>
#include <thread>
#include <vector>
#include <mutex>

// 加锁后，输出5000
class Wallet
{
    int mMoney;
    std::mutex mutex;

public:
    Wallet() : mMoney(0) {}
    int getMoney() { return mMoney; }
    void addMoney(int money)
    {
        std::lock_guard<std::mutex> lockGuard(mutex);
        for (int i = 0; i < money; i++)
        {
            mMoney++;
        }
        // mutex.unlock();
    }
};

int testMultithreadWallet()
{
    Wallet walletObject;
    std::vector<std::thread> threads;
    for (int i = 0; i < 5; ++i)
    {
        threads.push_back(std::thread(&Wallet::addMoney, &walletObject, 1000));
    }

    for (int i = 0; i < threads.size(); i++)
    {
        threads.at(i).join();
    }

    return walletObject.getMoney();
}

int main()
{
    int val = 0;
    for (int k = 0; k < 1000; k++)
    {
        if ((val = testMultithreadWallet()) != 5000)
        {
            std::cout << "Error at count= " << k << " money in wallet" << val << std::endl;
        }
    }
    std::cout << " money in wallet" << val << std::endl;

    return 0;
}
