#include <iostream>
#include <thread>

class DummyClass
{
public:
    DummyClass() {}
    DummyClass(const DummyClass &obj) {}
    void sampleMemberfunction(int x)
    {
        std::cout << "Inside sampleMemberfunction " << x << std::endl;
    }
};

int main()
{
    DummyClass dummyObj;
    int x = 10;
    std::thread threadObj(&DummyClass::sampleMemberfunction, &dummyObj, x);
    threadObj.join();

    return 0;
}
