#include <iostream>
#include <thread>
#include <algorithm>

/*
由于相同Wallet类对象的成员函数addMoney()执行了5次，所以money预计为50000，但由于addMoney()成员函数并行执行，因此在某些情况下，mMoney可能远小于5000
输出：
Error at count = 971  Money in Wallet = 4568
Error at count = 971  Money in Wallet = 4568
Error at count = 972  Money in Wallet = 4260
Error at count = 972  Money in Wallet = 4260
Error at count = 973  Money in Wallet = 4976
Error at count = 973  Money in Wallet = 4976
*/

class Wallet
{
    int mMoney;

public:
    Wallet() : mMoney(0) {}
    int getMoney() { return mMoney; }
    void addMoney(int money)
    {
        for (int i = 0; i < money; i++)
        {
            mMoney++;
        }
    }
};

int testMultithreadWallet()
{
    Wallet walletObject;
    std::vector<std::thread> threads;
    for (int i = 0; i < 5; i++)
    {
        threads.push_back(std::thread(&Wallet::addMoney, &walletObject, 1000));
    }

    for (int i = 0; i < 5; i++)
    {
        threads.at(i).join();
    }

    return walletObject.getMoney();
}

int main()
{
    int val = 0;
    for (int k = 0; k < 1000; k++)
    {
        if ((val == testMultithreadWallet()) != 5000)
        {
            std::cout << "Error at count = " << k << " Money in Wallet = " << val << std::endl;
        }
    }

    return 0;
}
