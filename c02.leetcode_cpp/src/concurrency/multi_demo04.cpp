#include <iostream>
#include <thread>
#include <mutex>

class Application
{
    std::mutex m_mutex;
    bool m_bDataLoaded;

public:
    Application()
    {
        m_bDataLoaded = false;
    }

    void loadData()
    {
        //使该线程sleep 1秒
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
        std::cout << "Load Dada from Xml" << std::endl;

        //锁定数据
        std::lock_guard<std::mutex> guard(m_mutex);

        // flag设为true，表明数据已加载
        m_bDataLoaded = true;
    }

    void mainTask()
    {
        std::cout << "Do some Handshaking" << std::endl;

        //获得锁
        m_mutex.lock();
        //检测flag是否设为true
        while (m_bDataLoaded != true)
        {
            //释放锁
            m_mutex.unlock();
            // sleep 100ms
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
            //获取锁
            m_mutex.lock();
        }
        m_mutex.unlock();
        //处理加载的数据
        std::cout << "Do Processing On loaded Data" << std::endl;
    }
};

/*
输出：
Do some Handshaking
Load Dada from Xml
Do Processing On loaded Data

该方法存在以下缺陷：
为了检测变量，线程将会持续获取-释放锁，这样会消耗CPU周期并且使线程1变慢，因为它需要获取相同的锁来更新bool变量。
因此，显然我们需要一个更好的实现机制，如某种方式，线程1可以通过等待event信号来阻塞，另一个线程可以通知该event并使线程1继续。这将会有相同的CPU周期，并有更好的性能。
*/

int main()
{
    Application app;

    std::thread thread_1(&Application::mainTask, &app);
    std::thread thread_2(&Application::loadData, &app);

    thread_2.join();
    thread_1.join();

    return 0;
}