#ifndef TEST_STUB_H__
#define TEST_STUB_H__
#include <stdio.h>
#include <string.h>
#include <vector>
#include <stdbool.h>
#include "util.h"

#define TAG_UT 1
#define null  -1

int ReadBinFile(const char *fileName, int *size, void *buf, int maxLimit);
int GenBinFile(const char *fileName, void *buf, int size);

bool isValidBST(struct TreeNode *root);
TreeNode *TreeInitByFullBfsInfo(const std::vector<int> &nodeInfo);
void PrintPreorder(TreeNode *root);
void PrintInorder(TreeNode *root);
void PrintPostorder(TreeNode *root);
void FreeTree(TreeNode *root);

ListNode *ListInit(const std::vector<int> &nodeInfo);
void PrintList(ListNode *root);
void FreeList(ListNode *root);

#endif // TEST_STUB_H__
