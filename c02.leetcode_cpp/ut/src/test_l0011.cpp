#include <iostream>
#include <gtest/gtest.h>
#include "test_stub.h"
#include "l0011.container_with_most_water.h"
#include <vector>

namespace LeetCode {
namespace Leet0011 {
class TestL0011 : public testing::Test {
protected:
    void SetUp() {

    }

    void TearDown() {

    }
};

#if TAG_UT
TEST(TestL0011, test0){
    std::vector<int> height {1,8,6,2,5,4,8,3,7};
    Solution solve;
    auto ret = solve.maxArea(height);
    EXPECT_EQ(49, ret);
}

TEST(TestL0011, test1){
    std::vector<int> height {1, 1};
    Solution solve;
    auto ret = solve.maxArea(height);
    EXPECT_EQ(1, ret);
}
#endif
}
}