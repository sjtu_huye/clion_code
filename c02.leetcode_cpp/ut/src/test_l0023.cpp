#include <iostream>
#include <gtest/gtest.h>
#include "test_stub.h"
#include "l0023.merge_k_sorted_lists.h"
#include <string>

namespace LeetCode {
namespace Leet0023 {
class TestL0023 : public testing::Test {
protected:
    void SetUp() {

    }

    void TearDown() {

    }
};

#if TAG_UT
TEST(TestL0023, test0){
    std::vector<int> dat0{1, 4, 5};
    std::vector<int> dat1{1, 3, 4};
    std::vector<int> dat2{2, 6};
    ListNode* head0 = ListInit(dat0);
    ListNode* head1 = ListInit(dat1);
    ListNode* head2 = ListInit(dat2);
    std::vector<ListNode*> inList;
    inList.push_back(head0);
    inList.push_back(head1);
    inList.push_back(head2);
    Solution solve;
    auto out = solve.mergeKLists(inList);
    PrintList(out);
    EXPECT_EQ(1, out->next->val);
    EXPECT_EQ(2, out->next->next->val);
    EXPECT_EQ(3, out->next->next->next->val);
    EXPECT_EQ(4, out->next->next->next->next->val);
    EXPECT_EQ(4, out->next->next->next->next->next->val);
    EXPECT_EQ(5, out->next->next->next->next->next->next->val);
    FreeList(out);
}

TEST(TestL0023, test1){
    std::vector<ListNode*> inList;
    Solution solve;
    auto out = solve.mergeKLists(inList);
    EXPECT_EQ(nullptr, out);
}

TEST(TestL0023, test2){
    std::vector<ListNode*> inList;
    ListNode* head = nullptr;
    inList.push_back(head);
    Solution solve;
    auto out = solve.mergeKLists(inList);
    EXPECT_EQ(nullptr, out);
}
#endif

}
}