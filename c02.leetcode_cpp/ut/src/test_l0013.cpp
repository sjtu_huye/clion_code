#include <iostream>
#include <gtest/gtest.h>
#include "test_stub.h"
#include "l0013.roman_to_integer.h"
#include <string>

namespace LeetCode {
namespace Leet0013 {
class TestL0013 : public testing::Test {
protected:
    void SetUp() {

    }

    void TearDown() {

    }
};

#if TAG_UT
TEST(TestL0013, test0){
    string s = "III";
    Solution solve;
    auto ret = solve.romanToInt(s);
    EXPECT_EQ(3, ret);
}

TEST(TestL0013, test1){
    string s = "IV";
    Solution solve;
    auto ret = solve.romanToInt(s);
    EXPECT_EQ(4, ret);
}

TEST(TestL0013, test2){
    string s = "IX";
    Solution solve;
    auto ret = solve.romanToInt(s);
    EXPECT_EQ(9, ret);
}

TEST(TestL0013, test3){
    string s = "LVIII";
    Solution solve;
    auto ret = solve.romanToInt(s);
    EXPECT_EQ(58, ret);
}

TEST(TestL0013, test4){
    string s = "MCMXCIV";
    Solution solve;
    auto ret = solve.romanToInt(s);
    EXPECT_EQ(1994, ret);
}
#endif

}
}