#include <iostream>
#include <gtest/gtest.h>
#include "test_stub.h"

namespace UtTest {
class TestDemo : public testing::Test {
protected:
    void SetUp() {

    }

    void TearDown() {

    }
};

int Add(int a, int b) {
    return a+b;
}

#if TAG_UT
TEST(TestDemo, test0){
    EXPECT_EQ(14, Add(4, 10));
}

TEST(TestDemo, test2){
    EXPECT_EQ(18, Add(8, 10)) << "right";
}
#endif

}
