#include "test_stub.h"
#include <errno.h>
#include <stdlib.h>
#ifndef _WIN32
#include <limits.h>
#endif

int ReadBinFile(const char *fileName, int *size, void *buf, int maxLimit)
{
    FILE *fp;
    int capacity = 0;
    fp = fopen(fileName, "rb");
    if (fp == nullptr) {
        printf("open fail errno = %d, reason = %s\n", errno, strerror(errno));
        return -1;
    }
    fseek(fp, 0, SEEK_END);
    capacity = ftell(fp);
    fseek(fp, 0, SEEK_SET);
    if (capacity > maxLimit) {
        printf("[ERROR]: file size exceed %d(%d)\n", maxLimit, capacity);
        fclose(fp);
        return -1;
    }
    if (buf) {
        size_t readCount = fread(buf, capacity, 1, fp);
        if (readCount != 1) {
            printf("[ERROR]: unable to read bin file %s\n", fileName);
            fclose(fp);
            return -1;
        }
    }
    fclose(fp);
    printf("Reading bin file %s ... Done. %d bytes\n", fileName, capacity);
    *size = capacity;
    return 0;
}

int GenBinFile(const char *fileName, void *buf, int size)
{
    FILE *fp;
    int capacity = 0;
    int status = 0;
    fp = fopen(fileName, "wb+");
    if (fp == nullptr) {
        printf("open fail errno = %d, reason = %s\n", errno, strerror(errno));
        return -1;
    }
    status = fwrite(buf, 1, size, fp);
    fclose(fp);
    if (status == 0) {
        printf("Could not write %s\n", fileName);
        return -1;
    }
    printf("Writing file %s ... Done. %d bytes\n", fileName, size);
    return 0;
}

long pre = LONG_MIN;
/*
INT_MIN:-2147483648
INT_MAX:2147483647
LONG_MIN:-2147483648
LONG_MAX:2147483647
LLONG_MIN:-9223372036854775808
LLONG_MAX:9223372036854775807
*/
// 校验左小于右，二叉搜索树
bool isValidBST(struct TreeNode *root)
{
    bool a = true, b = true, c = true;
    if (root)
    {
        a = isValidBST(root->left);
        b = pre < root->val;
        pre = root->val;
        c = isValidBST(root->right);
        return a && b && c;
    }
    return true;
}

template <typename T>
TreeNode *TreeCreateByFullBfsInfo(const T *inputArray, int n, int index)
{
    TreeNode *p = nullptr;
    if (index < n && inputArray[index] != null) {
        p = new TreeNode();
        p->val = inputArray[index];
        p->left = TreeCreateByFullBfsInfo(inputArray, n, 2 * index + 1);
        p->right = TreeCreateByFullBfsInfo(inputArray, n, 2 * index + 2);
    }
    return p;
}

// std::vector<int> dat{5,1,4,null,null,3,6};
TreeNode *TreeInitByFullBfsInfo(const std::vector<int> &nodeInfo)
{
    // 空节点则为null -1
    int size = nodeInfo.size();
    if (size == 0) {
        return nullptr;
    }
    TreeNode *root = TreeCreateByFullBfsInfo(nodeInfo.data(), size, 0);
    return root;
}

TreeNode *TreeInitByEffectBfsInfo(const std::vector<int> &nodeInfo)
{
    // 空节点则为null -1，输入形式为如果父节点存在，则告知子节点状况，或者全量信息
    int size = nodeInfo.size();
    if (size == 0) {
        return nullptr;
    }
    TreeNode *root = nullptr;
    // wait for implement
    return root;
}

void PrintPreorderRec(TreeNode *root)
{
    if (root) {
        printf("%d ", root->val);
        PrintPreorderRec(root->left);
        PrintPreorderRec(root->right);
    }
}

void PrintPreorder(TreeNode *root)
{
    printf("[TREE_PRE]: ");
    PrintPreorderRec(root);
    printf("\n");
}

void PrintInorderRec(TreeNode *root)
{
    if (root) {
        PrintInorderRec(root->left);
        printf("%d ", root->val);
        PrintInorderRec(root->right);
    }
}

void PrintInorder(TreeNode *root)
{
    printf("[TREE_IN]: ");
    PrintInorderRec(root);
    printf("\n");
}

void PrintPostorderRec(TreeNode *root)
{
    if (root) {
        PrintPostorderRec(root->left);
        PrintPostorderRec(root->right);
        printf("%d ", root->val);
    }
}

void PrintPostorder(TreeNode *root)
{
    printf("[TREE_POST]: ");
    PrintPostorderRec(root);
    printf("\n");
}

void FreeTree(TreeNode *root)
{
    if (root == nullptr) {
        return;
    }
    FreeTree(root->left);
    FreeTree(root->right);
    delete root;
    root = nullptr;
    return;
}

ListNode *ListInit(const std::vector<int> &nodeInfo)
{
    // 空节点则为null -1
    int size = nodeInfo.size();
    if (size == 0) {
        return nullptr;
    }
    ListNode *root = new ListNode();
    ListNode *p = root;
    root->val = nodeInfo[0];
    for (int i = 1; i < size; i++) {
        p->next = new ListNode();
        p = p->next;
        p->val = nodeInfo[i];
    }
    return root;
}

void PrintListRec(ListNode *root)
{
    if (root) {
        printf("%d ", root->val);
        PrintListRec(root->next);
    }
}

void PrintList(ListNode *root)
{
    printf("[LIST]: ");
    PrintListRec(root);
    printf("\n");
}

void FreeList(ListNode *root)
{
    if (root == nullptr) {
        return;
    }
    FreeList(root->next);
    delete root;
    root = nullptr;
    return;
}
