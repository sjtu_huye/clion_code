#include <iostream>
#include <string>
#include <gtest/gtest.h>
#include "test_stub.h"
#include "l0200.number_of_islands.h"

namespace LeetCode {
namespace Leet0200 {
class TestL0200 : public testing::Test {
protected:
    void SetUp() {

    }

    void TearDown() {

    }
};

#if TAG_UT
TEST(TestL0200, test0){
    std::vector<vector<char>> grid{
        {'1', '1', '1', '1', '0'},
        {'1', '1', '1', '1', '0'},
        {'1', '1', '1', '1', '0'},
        {'1', '1', '1', '1', '0'}};
    Solution solve;
    auto ret = solve.numIslands(grid);
    EXPECT_EQ(1, ret);
}

TEST(TestL0200, test1){
    std::vector<vector<char>> grid{
        {'1', '1', '1', '1', '0'},
        {'1', '1', '1', '1', '0'},
        {'1', '1', '1', '1', '0'},
        {'1', '1', '1', '1', '0'}};
    Solution solve;
    auto ret = solve.numIslands(grid);
    EXPECT_EQ(1, ret);
}
#endif

}
}