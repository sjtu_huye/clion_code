#include <iostream>
#include <gtest/gtest.h>
#include "test_stub.h"
#include "l0092.reverse_linked_list_ii.h"
#include <string>

namespace LeetCode {
namespace Leet0092 {
class TestL0092 : public testing::Test {
protected:
    void SetUp() {

    }

    void TearDown() {

    }
};

#if TAG_UT
TEST(TestL0092, test0){
    std::vector<int> dat{1, 2, 3, 4, 5};
    ListNode* head = ListInit(dat);
    PrintList(head);
    Solution solve;
    auto out = solve.reverseBetween(head, 2, 4);
    PrintList(out);
    EXPECT_EQ(4, out->next->val);
    EXPECT_EQ(3, out->next->next->val);
    EXPECT_EQ(2, out->next->next->next->val);
    EXPECT_EQ(5, out->next->next->next->next->val);
    FreeList(out);
}

TEST(TestL0092, test1){
    std::vector<int> dat{5};
    ListNode* head = ListInit(dat);
    Solution solve;
    auto out = solve.reverseBetween(head, 1, 1);
    EXPECT_EQ(5, out->val);
    FreeList(out);
}
#endif

}
}