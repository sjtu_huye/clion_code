#include <iostream>
#include <gtest/gtest.h>
#include "test_stub.h"
#include "l0012.integer_to_roman.h"
#include <string>

namespace LeetCode {
namespace Leet0012 {
class TestL0012 : public testing::Test {
protected:
    void SetUp() {

    }

    void TearDown() {

    }
};

#if TAG_UT
TEST(TestL0012, test0){
    int num = 3;
    Solution solve;
    auto ret = solve.intToRoman(num);
    EXPECT_EQ("III", ret);
}

TEST(TestL0012, test1){
    int num = 4;
    Solution solve;
    auto ret = solve.intToRoman(num);
    EXPECT_EQ("IV", ret);
}

TEST(TestL0012, test2){
    int num = 9;
    Solution solve;
    auto ret = solve.intToRoman(num);
    EXPECT_EQ("IX", ret);
}

TEST(TestL0012, test3){
    int num = 58;
    Solution solve;
    auto ret = solve.intToRoman(num);
    EXPECT_EQ("LVIII", ret);
}
#endif

}
}