#include <iostream>
#include <gtest/gtest.h>
#include "test_stub.h"
#include "l0958.check_completeness_of_a_binary_tree.h"

namespace LeetCode {
namespace Leet0958 {
class TestL0958 : public testing::Test {
protected:
    void SetUp() {

    }

    void TearDown() {

    }
};

#if TAG_UT
TEST(TestL0958, test0){
    std::vector<int> dat{1, 2, 3, 4, 5, 6};
    TreeNode *root = TreeInitByFullBfsInfo(dat);
    PrintPreorder(root);
    PrintInorder(root);
    PrintPostorder(root);
    Solution solve;
    auto ret = solve.isCompleteTree(root);
    EXPECT_EQ(true, ret);
    FreeTree(root);
}

TEST(TestL0958, test1){
    std::vector<int> dat{1, 2, 3, 4, 5, null, 7};
    TreeNode *root = TreeInitByFullBfsInfo(dat);
    PrintPreorder(root);
    Solution solve;
    auto ret = solve.isCompleteTree(root);
    EXPECT_EQ(false, ret);
    FreeTree(root);
}
#endif

}
}