#include <iostream>
#include <gtest/gtest.h>
#include "test_stub.h"
#include "l0129.sum_root_to_leaf_numbers.h"

namespace LeetCode {
namespace Leet0129 {
class TestL0129 : public testing::Test {
protected:
    void SetUp() {

    }

    void TearDown() {

    }
};

#if TAG_UT
TEST(TestL0129, test0){
    std::vector<int> dat{1, 2, 3};
    TreeNode *root = TreeInitByFullBfsInfo(dat);
    PrintPreorder(root);
    Solution solve;
    auto ret = solve.sumNumbers(root);
    EXPECT_EQ(25, ret);
    FreeTree(root);
}

TEST(TestL0129, test1){
    std::vector<int> dat{4, 9, 0, 5, 1};
    TreeNode *root = TreeInitByFullBfsInfo(dat);
    PrintPreorder(root);
    Solution solve;
    auto ret = solve.sumNumbers(root);
    EXPECT_EQ(1026, ret);
    FreeTree(root);
}
#endif

}
}