#include <iostream>
#include <gtest/gtest.h>
#include "test_stub.h"
#include "l0143.reorder_list.h"
#include <string>

namespace LeetCode {
namespace Leet0143 {
class TestL0143 : public testing::Test {
protected:
    void SetUp() {

    }

    void TearDown() {

    }
};

#if TAG_UT
TEST(TestL0143, test0){
    int a[5] = {1,2,3,4};
    std::vector<int> dat(a, a+4);
    ListNode* head = ListInit(dat);
    PrintList(head);
    Solution solve;
    solve.reorderList(head);
    PrintList(head);
    EXPECT_EQ(4, head->next->val);
    EXPECT_EQ(2, head->next->next->val);
    EXPECT_EQ(3, head->next->next->next->val);
    FreeList(head);
}

TEST(TestL0143, test1){
    std::vector<int> dat{1, 2, 3, 4, 5};
    ListNode* head = ListInit(dat);
    PrintList(head);
    Solution solve;
    solve.reorderList(head);
    PrintList(head);
    EXPECT_EQ(5, head->next->val);
    EXPECT_EQ(2, head->next->next->val);
    EXPECT_EQ(4, head->next->next->next->val);
    EXPECT_EQ(3, head->next->next->next->next->val);
    FreeList(head);
}
#endif

}
}