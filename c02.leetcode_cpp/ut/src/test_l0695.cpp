#include <iostream>
#include <string>
#include <gtest/gtest.h>
#include "test_stub.h"
#include "l0695.max_area_of_island.h"

namespace LeetCode {
namespace Leet0695 {
class TestL0695 : public testing::Test {
protected:
    void SetUp() {

    }

    void TearDown() {

    }
};

#if TAG_UT
TEST(TestL0695, test0){
    std::vector<vector<int>> grid{
        {0,0,1,0,0,0,0,1,0,0,0,0,0},
        {0,0,0,0,0,0,0,1,1,1,0,0,0},
        {0,1,1,0,1,0,0,0,0,0,0,0,0},
        {0,1,0,0,1,1,0,0,1,0,1,0,0},
        {0,1,0,0,1,1,0,0,1,1,1,0,0},
        {0,0,0,0,0,0,0,0,0,0,1,0,0},
        {0,0,0,0,0,0,0,1,1,1,0,0,0},
        {0,0,0,0,0,0,0,1,1,0,0,0,0}};
    Solution solve;
    auto ret = solve.maxAreaOfIsland(grid);
    EXPECT_EQ(6, ret);
}

TEST(TestL0695, test1){
    std::vector<vector<int>> grid{{0,0,0,0,0,0,0,0}};
    Solution solve;
    auto ret = solve.maxAreaOfIsland(grid);
    EXPECT_EQ(0, ret);
}
#endif

}
}