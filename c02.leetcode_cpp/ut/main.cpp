/*
 * @Description
 * @Author Admin
 * @Date 2020/2/23
 * @Copyright(C) 2020 .China .all right reserved
 */

#include <iostream>
#include <gtest/gtest.h>

int main(int argc, char ** argv) {
    std::cout << "Hello, World!" << std::endl;
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();  // 执行所有的 test case
}
