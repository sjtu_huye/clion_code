/*
 * Created by sjtu_huye on 2021/4/26.
 * Copyright (c) 2021-2021 sjtu_huye@person.com , All rights reserved.
 */

/*
给定一个整数数组 nums 和一个整数目标值 target，请你在该数组中找出 和为目标值 的那 两个 整数，并返回它们的数组下标。
你可以假设每种输入只会对应一个答案。但是，数组中同一个元素在答案里不能重复出现。
你可以按任意顺序返回答案。

示例 1：
输入：nums = [2,7,11,15], target = 9
输出：[0,1]
解释：因为 nums[0] + nums[1] == 9 ，返回 [0, 1] 。

链接：https://leetcode-cn.com/problems/two-sum
*/

#include <stdio.h>
#include <malloc.h>
#include <math.h>
#include "stdout_proc.h"


/**
 * Note: The returned array must be malloced, assume caller calls free().
 */
int* twoSum0(int* nums, int numsSize, int target, int* returnSize){
    int *outPos = malloc(sizeof(int) * 2);
    *returnSize = 2;
    for (int i = 0; i < numsSize - 1; ++i) {
        for (int j = i + 1; j < numsSize; ++j) {
            if (nums[i] + nums[j] == target) {
                outPos[0] = i;
                outPos[1] = j;
                return outPos;
            }
        }
    }
    return outPos;
}

//链地址法
#define HashSize 10000
struct list{
    int key;
    int val;
    struct list *next;
};

typedef struct MyHashMap{
    struct list *date;
}MyHashMap;

//向哈希表中插入
void ListPush(struct list *head,int key,int val){
    struct list *tmp=(struct list *)malloc(sizeof(struct list));
    tmp->key=key;
    tmp->val=val;
    tmp->next=head->next;
    head->next=tmp;
}
//哈希表中查找
struct list *ListFind(struct list *head,int val){
    for(struct list *it=head;it->next;it=it->next){
        if(it->next->val==val){
            return it->next;
        }
    }
    return NULL;
}
//释放空间
void ListFree(struct list *head){
    while(head->next){
        struct list *tmp=head->next;
        head->next=tmp->next;
        free(tmp);
    }
}
MyHashMap *CreateHash(){
    //初始化哈希表
    MyHashMap *obj=(MyHashMap *)malloc(sizeof(MyHashMap));
    obj->date=malloc(sizeof(struct list)*HashSize);
    for(int i=0;i<HashSize;++i){
        obj->date[i].val=0;
        obj->date[i].key=0;
        obj->date[i].next=NULL;
    }
    return obj;
}
//散列函数
int hash(int val){
    return fabs(val%HashSize);
}

int* twoSum(int* nums, int numsSize, int target, int* returnSize){
    //要返回的数组
    int *ret=(int *)malloc(sizeof(int)*2);
    struct MyHashMap *obj=CreateHash();

//开始遍历数组
    for(int i=0;i<numsSize;++i){
        int h=hash(target-nums[i]);
        struct list *p=ListFind(&(obj->date[h]),target-nums[i]);
        if(p){
            ret[0]=p->key;
            ret[1]=i;
            *returnSize=2;
            //释放内存
            for(int j=0;j<HashSize;j++){
                ListFree(&(obj->date[j]));
            }
            free(obj);
            return ret;
            break;
        }
        else{
            h=hash(nums[i]);
            ListPush(&(obj->date[h]),i,nums[i]);
        }
    }
    for(int i=0;i<HashSize;i++){
        ListFree(&(obj->date[i]));
    }
    free(obj);
    *returnSize=0;
    return NULL;
}

#define INPUT_ARR_SIZE 100
int main() {
    // int nums[] = {2,7,11,15};
    // int numSize = sizeof(nums)/sizeof(int);
    // int target = 9;
    // int returnSize = 0;
    // int *outNum = twoSum(nums, numSize, target, &returnSize);
    // printf("[OUT]: %d, %d\n", outNum[0], outNum[1]);
    // free(outNum);
    char input[INPUT_ARR_SIZE];
    int nums[INPUT_ARR_SIZE];
    int target = 0;
    int numSize = 0;
    int caseCont = 0;
    int returnSize = 0;
    while (scanf("%s %d", input, &target)!=EOF) {
        printf("\n[case %d]: \n", caseCont);
        caseCont++;
        OneDimensionalMatrixStrToIntArr(input, nums, &numSize);
        printf("[target]: %d\n", target);

        int *outNum = twoSum(nums, numSize, target, &returnSize);
        printf("[OUT]: %d, %d\n", outNum[0], outNum[1]);
        free(outNum);
    }
    return 0;

}