/*
 * Created by sjtu_huye on 2021/5/5.
 * Copyright (c) 2021-2021 sjtu_huye@person.com , All rights reserved.
 */

/*
给你一个由 '1'（陆地）和 '0'（水）组成的的二维网格，请你计算网格中岛屿的数量。
岛屿总是被水包围，并且每座岛屿只能由水平方向和/或竖直方向上相邻的陆地连接形成。
此外，你可以假设该网格的四条边均被水包围。

示例 1：
输入：grid = [
  ["1","1","1","1","0"],
  ["1","1","0","1","0"],
  ["1","1","0","0","0"],
  ["0","0","0","0","0"]
]
输出：1
示例 2：
输入：grid = [
  ["1","1","0","0","0"],
  ["1","1","0","0","0"],
  ["0","0","1","0","0"],
  ["0","0","0","1","1"]
]
输出：3
 
提示：
m == grid.length
n == grid[i].length
1 <= m, n <= 300
grid[i][j] 的值为 '0' 或 '1'

链接：https://leetcode-cn.com/problems/number-of-islands
*/


#include <stdio.h>
#include <malloc.h>
#include "stdout_proc.h"


int numIslands(char** grid, int gridSize, int* gridColSize){
    return 0;
}


#define INPUT_ARR_SIZE 100
int main() {
    // char input[][5] = {
    //     {"1","1","1","1","0"},
    //     {"1","1","0","1","0"},
    //     {"1","1","0","0","0"},
    //     {"0","0","0","0","0"}
    // };
    // int rowSize = 4;
    // int *colSize = malloc(sizeof(int) * 4);
    // for (int i = 0; i < 4; i++)
    // {
    //     colSize[i] = 5;
    // }
    // int out = numIslands(input, rowSize, colSize);
    // printf("[OUT]: %d\n", out);
    
    return 0;
}