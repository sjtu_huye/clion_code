msys2安装完需要配置环境变量：
MSYS_DIR=C:\software\msys2
MINGW64_DIR=C:\software\msys2\mingw64
Path添加: %MSYS_DIR%\usr\bin;%MINGW64_DIR%\bin

--base-devl
pacman -Syu
pacman -S mingw-w64-x86_64-gcc mingw-w64-x86_64-gdb mingw-w64-x86_64-cmake mingw-w64-x86_64-make
or:
pacman -S mingw-w64-x86_64-gcc mingw-w64-x86_64-gdb mingw-w64-x86_64-make
pacman -S mingw-w64-x86_64-toolchain
pacman -S mingw-w64-x86_64-clang
pacman -S mingw-w64-x86_64-yasm mingw-w64-x86_64-nasm
pacman -S mingw-w64-x86_64-freetype
#下面几条命令根据需求来，装自己想要的
pacman -S mingw-w64-x86_64-opencv
pacman -S mingw-w64-x86_64-ffmpeg mingw-w64-x86_64-ffms2
pacman -S mingw-w64-x86_64-libwebsockets


"terminal.integrated.shell.windows": "C:/software/msys2/usr/bin/bash.exe"
"cmake.cmakePath": "C:/software/cmake/bin/cmake.exe",
"cmake.configureOnOpen": true,
"cmake.generator": "MinGW Makefiles"

gitlab 搭建ci
Jenkins：https://www.jianshu.com/p/5f671aca2b5a