/*
 * Created by sjtu_huye on 2021/5/5.
 * Copyright (c) 2021-2021 sjtu_huye@person.com , All rights reserved.
 */

/*
给定两个大小分别为 m 和 n 的正序（从小到大）数组 nums1 和 nums2。请你找出并返回这两个正序数组的 中位数 。

示例 1：
输入：nums1 = [1,3], nums2 = [2]
输出：2.00000
解释：合并数组 = [1,2,3] ，中位数 2
示例 2：
输入：nums1 = [1,2], nums2 = [3,4]
输出：2.50000
解释：合并数组 = [1,2,3,4] ，中位数 (2 + 3) / 2 = 2.5
示例 3
输入：nums1 = [0,0], nums2 = [0,0]
输出：0.00000
示例 4：
输入：nums1 = [], nums2 = [1]
输出：1.00000
示例 5：
输入：nums1 = [2], nums2 = []
输出：2.00000
 

提示：
nums1.length == m
nums2.length == n
0 <= m <= 1000
0 <= n <= 1000

链接：https://leetcode-cn.com/problems/median-of-two-sorted-arrays
*/

#include <stdio.h>
#include <malloc.h>
#include "stdout_proc.h"

// 优化内存，无需申请内存
double findMedianSortedArrays(int* nums1, int nums1Size, int* nums2, int nums2Size) {
    int i, j, k, l;
    int sumLen = nums1Size + nums2Size;
    int midL = 0;
    int midR = 0;
    int midPos = (sumLen + 1)/2;
    // 后面判决条件做了过滤，同时不存在都为0情况，所以可以不做分支判断，但是为了提速，所以增加下面处理
    if(nums1Size == 0) {
        return (nums2[((nums2Size - 1)/2)] + nums2[nums2Size/2]) / 2.0;
    }
    if(nums2Size == 0) {
        return (nums1[((nums1Size - 1)/2)] + nums1[nums1Size/2]) / 2.0;
    }
    if (sumLen % 2 == 0) {
        midPos = sumLen/2 + 1;
    }
    for (i = 0, k = 0, l = 0; i < midPos; i++)
    {
        midL = midR;
        if(k < nums1Size) {
            if(l < nums2Size) {
                if(nums1[k] <= nums2[l]) {
                    midR = nums1[k];
                    k++;
                } else {
                    midR = nums2[l];
                    l++;
                }
            } else {
                midR = nums1[k];
                k++;
            }
        } else {
            midR = nums2[l];
            l++;
        }
    }
    if(sumLen % 2 == 0) {
        return (midL + midR) / 2.0;
    }
    return midR;
}

double findMedianSortedArrays2(int* nums1, int nums1Size, int* nums2, int nums2Size) {
    int i, j, k, l;
    int sumLen = nums1Size + nums2Size;
    double out = 0;
    // 后面判决条件做了过滤，同时不存在都为0情况，但是为了提速，所以增加下面处理
    if(nums1Size == 0) {
        return (nums2[((nums2Size - 1)/2)] + nums2[nums2Size/2]) / 2.0;
    }
    if(nums2Size == 0) {
        return (nums1[((nums1Size - 1)/2)] + nums1[nums1Size/2]) / 2.0;
    }
    int *numSum = (int *)malloc(sizeof(int) * (sumLen));
    for (i = 0, k = 0, l = 0; i < sumLen; i++)
    {
        if(k < nums1Size) {
            if(l < nums2Size) {
                if(nums1[k] <= nums2[l]) {
                    numSum[i] = nums1[k];
                    k++;
                } else {
                    numSum[i] = nums2[l];
                    l++;
                }
            } else {
                numSum[i] = nums1[k];
                k++;
            }
        } else {
            numSum[i] = nums2[l];
            l++;
        }
    }
    out = (numSum[((sumLen - 1)/2)] + numSum[sumLen/2]) / 2.0;
    free(numSum);
    return out;
}


#define INPUT_ARR_SIZE 100
int main() {
    // int input1[] = {1,2,3};
    // int input2[] = {4,5};
    // int lenL1 = sizeof(input1)/sizeof(int);
    // int lenL2 = sizeof(input2)/sizeof(int);
    // double out = findMedianSortedArrays(input1, lenL1, input2, lenL2)

    char inputStr1[INPUT_ARR_SIZE];
    char inputStr2[INPUT_ARR_SIZE];
    int input1[INPUT_ARR_SIZE];
    int input2[INPUT_ARR_SIZE];
    int lenL1 = 0;
    int lenL2 = 0;
    double out = 0;
    int caseCont = 0;

    while (scanf("%s %s", inputStr1, inputStr2)!=EOF) {
        printf("\n[case %d]: \n", caseCont);
        caseCont++;
        OneDimensionalMatrixStrToIntArr(inputStr1, input1, &lenL1);
        OneDimensionalMatrixStrToIntArr(inputStr2, input2, &lenL2);

        out = findMedianSortedArrays(input1, lenL1, input2, lenL2);
        printf("[OUT]: %f\n", out);
    }
    return 0;
}