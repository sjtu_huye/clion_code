/*
 * Created by sjtu_huye on 2021/5/5.
 * Copyright (c) 2021-2021 sjtu_huye@person.com , All rights reserved.
 */

/*
给你一个整数数组 nums ，你可以对它进行一些操作。
每次操作中，选择任意一个 nums[i] ，删除它并获得 nums[i] 的点数。之后，你必须删除每个等于 nums[i] - 1 或 nums[i] + 1 的元素。
开始你拥有 0 个点数。返回你能通过这些操作获得的最大点数。

示例 1：
输入：nums = [3,4,2]
输出：6
解释：
删除 4 获得 4 个点数，因此 3 也被删除。
之后，删除 2 获得 2 个点数。总共获得 6 个点数。
示例 2：
输入：nums = [2,2,3,3,3,4]
输出：9
解释：
删除 3 获得 3 个点数，接着要删除两个 2 和 4 。
之后，再次删除 3 获得 3 个点数，再次删除 3 获得 3 个点数。
总共获得 9 个点数。

提示：
1 <= nums.length <= 2 * 104
1 <= nums[i] <= 104

链接：https://leetcode-cn.com/problems/delete-and-earn
*/


#include <stdio.h>
#include <malloc.h>
#include "stdout_proc.h"


int deleteAndEarn(int* nums, int numsSize){
    return 0;
}


#define INPUT_ARR_SIZE 100
int main() {
    // int nums[] = {2,7,11,15};
    // int numSize = sizeof(nums)/sizeof(int);
    // int out = deleteAndEarn(nums, numSize);
    // printf("[OUT]: %d\n", out);
    char input[INPUT_ARR_SIZE];
    int nums[INPUT_ARR_SIZE];
    int numSize = 0;
    int out = 0;
    int caseCont = 0;
    while (scanf("%s", input)!=EOF) {
        printf("\n[case %d]: \n", caseCont);
        caseCont++;
        OneDimensionalMatrixStrToIntArr(input, nums, &numSize);

        out = deleteAndEarn(nums, numSize);
        printf("[OUT]: %d\n", out);
    }
    return 0;
}