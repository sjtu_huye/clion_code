/*
 * Created by sjtu_huye on 2021/4/24.
 * Copyright (c) 2021-2021 sjtu_huye@person.com , All rights reserved.
 */

/*
给你两个长度相同的字符串，s 和 t。
将 s 中的第 i 个字符变到 t 中的第 i 个字符需要 |s[i] - t[i]| 的开销（开销可能为 0），也就是两个字符的 ASCII 码值的差的绝对值。
用于变更字符串的最大预算是 maxCost。在转化字符串时，总开销应当小于等于该预算，这也意味着字符串的转化可能是不完全的。
如果你可以将 s 的子字符串转化为它在 t 中对应的子字符串，则返回可以转化的最大长度。
如果 s 中没有子字符串可以转化成 t 中对应的子字符串，则返回 0。

示例 1：
输入：s = "abcd", t = "bcdf", maxCost = 3
输出：3
解释：s 中的 "abc" 可以变为 "bcd"。开销为 3，所以最大长度为 3。
示例 2：
输入：s = "abcd", t = "cdef", maxCost = 3
输出：1
解释：s 中的任一字符要想变成 t 中对应的字符，其开销都是 2。因此，最大长度为 1。
示例 3：
输入：s = "abcd", t = "acde", maxCost = 0
输出：1
解释：a -> a, cost = 0，字符串未发生变化，所以最大长度为 1。

提示：
1 <= s.length, t.length <= 10^5
0 <= maxCost <= 10^6
s 和 t 都只含小写英文字母。

来源：力扣（LeetCode）
链接：https://leetcode-cn.com/problems/get-equal-substrings-within-budget
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>

int compare(const void *a,const void *b) {
        return *(int*)a-*(int*)b;
}
// qsort(cosArr,strLen,sizeof(int),compare);

int equalSubstring0(char * s, char * t, int maxCost){
    //if (maxCost == 0) {
    //    return 0;
    //}
    int i, j;
    int strLen = strlen(s);
    int *cosArr = (int *)malloc(sizeof(int)*strLen);
    for (i = 0; i < strLen; ++i) {
        cosArr[i] = abs(s[i] -t[i]);
    }
    int outLen = 0;
    int sumCos = 0;
    for (i = 0; i < strLen; ++i) {
        sumCos = 0;
        for (j = i; j < strLen; ++j) {
            if((sumCos + cosArr[j]) > maxCost) {
                break;
            }
            sumCos += cosArr[j];
        }
        outLen = outLen < (j - i) ? (j - i) : outLen; // j已+1
        if (j == strLen){
            return outLen;
        }
    }
    return outLen;
}


int equalSubstring1(char * s, char * t, int maxCost){
    //if (maxCost == 0) {
    //    return 0;
    //}
    int i, j;
    int strLen = strlen(s);
    int *cosArr = (int *)malloc(sizeof(int)*strLen);
    for (i = 0; i < strLen; ++i) {
        cosArr[i] = abs(s[i] -t[i]);
    }
    int outLen = 0;
    int sumCos = 0;
    int lastPos = 0;
    for (i = 0; i < strLen; ++i) {
        if (i != 0) {sumCos -= cosArr[i-1];}  // sumCos = 0;
        for (j = lastPos; j < strLen; ++j) {
            if((sumCos + cosArr[j]) > maxCost) {
                break;
            }
            sumCos += cosArr[j];
        }
        lastPos = j;
        outLen = outLen < (j - i) ? (j - i) : outLen; // j已+1
        if (j == strLen){
            return outLen;
        }
    }
    return outLen;
}

int equalSubstring(char * s, char * t, int maxCost){
    //if (maxCost == 0) {
    //    return 0;
    //}
    int i, j;
    int strLen = strlen(s);
    int outLen = 0;
    int sumCos = 0;
    int lastPos = 0;
    for (i = 0; i < strLen; ++i) {
        if (i != 0) {sumCos -= abs(s[i-1] - t[i-1]);}  // sumCos = 0;
        for (j = lastPos; j < strLen; ++j) {
            if((sumCos + abs(s[j] - t[j])) > maxCost) {
                break;
            }
            sumCos += abs(s[j] - t[j]);
        }
        lastPos = j;
        outLen = outLen < (j - i) ? (j - i) : outLen; // j已+1
        if (j == strLen){
            return outLen;
        }
    }
    return outLen;
}

#define INPUT_ARR_SIZE 100
int main() {
    // char s[] = "abcd";
    // char t[] = "bcdf";
    // int cos = 2;
    // out = equalSubstring(s, t, cos);
    // printf("[OUT]: %d\n", out);
    char s[INPUT_ARR_SIZE];
    char t[INPUT_ARR_SIZE];
    int cos = 0;
    int out = 0;
    int caseCont = 0;
    while (scanf("%s %s %d", s, t, &cos)!=EOF){
        printf("[case %d]: \n", caseCont);
        caseCont++;
        printf("s: %s\n", s);
        printf("t: %s\n", t);
        printf("cos: %d\n", cos);
        out = equalSubstring(s, t, cos);
        printf("[OUT]: %d\n", out);
    }
    return 0;
}