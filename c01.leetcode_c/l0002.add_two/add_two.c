/*
 * Created by sjtu_huye on 2021/5/2.
 * Copyright (c) 2021-2021 sjtu_huye@person.com , All rights reserved.
 */

/*
给你两个 非空 的链表，表示两个非负的整数。它们每位数字都是按照 逆序 的方式存储的，并且每个节点只能存储 一位 数字。
请你将两个数相加，并以相同形式返回一个表示和的链表。
你可以假设除了数字 0 之外，这两个数都不会以 0 开头。

示例 1：
输入：l1 = [2,4,3], l2 = [5,6,4]
输出：[7,0,8]
解释：342 + 465 = 807.
示例 2：
输入：l1 = [0], l2 = [0]
输出：[0]
示例 3：
输入：l1 = [9,9,9,9,9,9,9], l2 = [9,9,9,9]
输出：[8,9,9,9,0,0,0,1]
 
提示：
每个链表中的节点数在范围 [1, 100] 内
0 <= Node.val <= 9
题目数据保证列表表示的数字不含前导零

https://leetcode-cn.com/problems/add-two-numbers
*/

#include <stdio.h>
#include <malloc.h>
#include <math.h>
#include "stdout_proc.h"

struct ListNode {
    int val;
    struct ListNode *next;
};

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     struct ListNode *next;
 * };
 */

#define MAX_LEN 102

void GetNumInfoFromBack(struct ListNode* l, int *num, int *len) {
    struct ListNode* tmp = l;
    int cont = 0;
    while(tmp) {
        num[MAX_LEN - cont - 1] = tmp->val;
        tmp = tmp->next;
        cont++;
    }
    // while(tmp) {
    //     tmp = tmp->next;
    //     cont++;
    // }
    // tmp = l;
    // for (int i = 0; i < cont; i++)
    // {
    //     num[MAX_LEN - cont + i] = tmp->val;
    //     tmp = tmp->next;
    // }
    *len = cont;
}

struct ListNode* addTwoNumbers(struct ListNode* l1, struct ListNode* l2) {
    int numL1[MAX_LEN] = {0};
    int numL2[MAX_LEN] = {0};
    int numOut[MAX_LEN] = {0};
    int lenL1 = 0;
    int lenL2 = 0;
    int maxLen = 0;
    int pos = 0;
    int add = 0;
    int tmp = 0;
    int i = 0;
    struct ListNode *node = NULL;
    struct ListNode *out = NULL;

    GetNumInfoFromBack(l1, numL1, &lenL1);
    GetNumInfoFromBack(l2, numL2, &lenL2);
    maxLen = (lenL1 > lenL2) ? lenL1 : lenL2;
    for (i = 0; i < maxLen; i++)
    {
        pos = MAX_LEN - 1 - i;
        tmp = numL1[pos] + numL2[pos] + add;
        numOut[pos] = (tmp) % 10;
        add = tmp / 10;
    }

    if (add != 0) {
        maxLen++;
        numOut[MAX_LEN - maxLen] = add;
    }
    out = malloc(sizeof(struct ListNode));
    node = out;
    for (i = 0; i < maxLen; i++)
    {
        node->val = numOut[MAX_LEN - 1 - i];
        if(i == maxLen - 1){
            node->next = NULL;
        } else {
            node->next = malloc(sizeof(struct ListNode));
        }
        node = node->next;
    }

    return out;
}


void ListNodeInit(int *input, int len, struct ListNode *l) {
    for (int i = 0; i < len; i++)
    {
        l->val = input[i];
        if (i != len - 1)
        {
            l->next = malloc(sizeof(struct ListNode));
        } else {
            l->next = NULL;
        }
        
    }
}

void ListNodeFree(struct ListNode *l) {
    struct ListNode *freeNode = NULL;
    while(l) {
        freeNode = l;
        l = l->next;
        free(freeNode);
    }
}


#define INPUT_ARR_SIZE 100
int main() {
    // int input1[] = {2,4,3};
    // int input2[] = {5,6,4};
    // int lenL1 = sizeof(input1)/sizeof(int);
    // int lenL2 = sizeof(input2)/sizeof(int);
    // struct ListNode *l1 = malloc(sizeof(struct ListNode));
    // struct ListNode *l2 = malloc(sizeof(struct ListNode));
    // struct ListNode *out = NULL;
    // struct ListNode *outPrint = NULL;
    // ListNodeInit(input1, lenL1, l1);
    // ListNodeInit(input2, lenL2, l2);
    // out = addTwoNumbers(l1, l2);
    // ListNodeFree(l1);
    // ListNodeFree(l2);
    // outPrint = out;
    // while(outPrint) {
    //     printf("%d ", outPrint->val);
    //     outPrint = outPrint->next;
    // }
    // ListNodeFree(out);

    char inputStr1[INPUT_ARR_SIZE];
    char inputStr2[INPUT_ARR_SIZE];
    int input1[INPUT_ARR_SIZE];
    int input2[INPUT_ARR_SIZE];
    int lenL1 = 0;
    int lenL2 = 0;

    struct ListNode *l1 = malloc(sizeof(struct ListNode));
    struct ListNode *l2 = malloc(sizeof(struct ListNode));
    struct ListNode *out = NULL;
    struct ListNode *outPrint = NULL;
    int caseCont = 0;

    while (scanf("%s %s", inputStr1, inputStr2)!=EOF) {
        printf("\n[case %d]: \n", caseCont);
        caseCont++;
        OneDimensionalMatrixStrToIntArr(inputStr1, input1, &lenL1);
        OneDimensionalMatrixStrToIntArr(inputStr2, input2, &lenL2);
        ListNodeInit(input1, lenL1, l1);
        ListNodeInit(input2, lenL2, l2);

        out = addTwoNumbers(l1, l2);
        ListNodeFree(l1);
        ListNodeFree(l2);
        outPrint = out;
        while(outPrint) {
            printf("%d ", outPrint->val);
            outPrint = outPrint->next;
        }
        ListNodeFree(out);
    }
    return 0;
}