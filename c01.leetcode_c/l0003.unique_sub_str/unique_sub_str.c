/*
 * Created by sjtu_huye on 2021/5/3.
 * Copyright (c) 2021-2021 sjtu_huye@person.com , All rights reserved.
 */

/*
给定一个字符串，请你找出其中不含有重复字符的 最长子串 的长度。

示例 1:
输入: s = "abcabcbb"
输出: 3 
解释: 因为无重复字符的最长子串是 "abc"，所以其长度为 3。
示例 2:
输入: s = "bbbbb"
输出: 1
解释: 因为无重复字符的最长子串是 "b"，所以其长度为 1。
示例 3:
输入: s = "pwwkew"
输出: 3
解释: 因为无重复字符的最长子串是 "wke"，所以其长度为 3。
     请注意，你的答案必须是 子串 的长度，"pwke" 是一个子序列，不是子串。
示例 4:
输入: s = ""
输出: 0
 
提示：
0 <= s.length <= 5 * 104
s 由英文字母、数字、符号和空格组成

链接：https://leetcode-cn.com/problems/longest-substring-without-repeating-characters
*/

#include <stdio.h>
#include <malloc.h>
#include <math.h>
#include <string.h>
#include "stdout_proc.h"

#define MAX_CHARACTER_NUM 256


/*
思路1：
记录方式：变量出现次数
左右滑窗，右侧先滑，滑到存在重复记录，则停止，开始左滑一格，如果还重复，则继续左滑，直到不重复，则继续右滑，
最终到达右侧末端，不重复则停止，如果右侧剩余数量比最大记录还小则不用继续了（优化点）

思路2：
从左开始将出现字符和位置记录，不存在则记录，存在，则左侧起始点定位成重复的第一个点后一个位置（因为之前的缓存已经没有意义了，不会更大了），然后继续右滑，直到结束
*/

int lengthOfLongestSubstring1(char * s) {
    int character[MAX_CHARACTER_NUM] = {0};
    int endPos = 0;
    int maxLen = 0;
    int len = strlen(s);
    for (int i = 0; i < len; i++)
    {
        if(i != 0) {
            character[s[i-1]]--;  // 可以构造成二维，一个记录当前数目，一个记录前一个记录的位置，这样左侧可以直接滑过，但是也需要消除已有记录，维持目前方式
        }
        for (int j = endPos; j < len; j++)
        {
            if(character[s[j]] == 1) {
                endPos = j;
                maxLen = (maxLen < (j - i)) ? (j - i) : maxLen;  // 当前j不考虑
                // printf("[%d, %d, %d] wait start move!\n", i, j, maxLen);
                break;
            }
            character[s[j]]++;
            if (len - i < maxLen) { // 优化点
                return maxLen;
            }
            if(j == len -1) {
                return (maxLen < (j - i + 1)) ? (j - i + 1) : maxLen;
            }
        }
    }
    return maxLen;
}

int lengthOfLongestSubstring(char * s) {
    int character[MAX_CHARACTER_NUM] = {0};
    int endPos = 0;
    int maxLen = 0;
    int len = strlen(s);
    int j;
    for (int i = 0; i < MAX_CHARACTER_NUM; ++i) {
        character[i] = -1;
    }
    for (int i = 0; i < len;)
    {
        for (int j = endPos; j < len; j++)
        {
            if(character[s[j]] >= i) {  // 说明在范围内
                endPos = j + 1;
                maxLen = (maxLen < (j - i)) ? (j - i) : maxLen;  // 当前j不考虑
                // printf("[%d, %d, %d] wait start move!\n", i, j, maxLen);
                i = character[s[j]] + 1;
                character[s[j]] = j;
                break;
            }
            character[s[j]] = j;
        }
        if (len - i < maxLen) { // 优化点
            return maxLen;
        }
        if(j == len) {  // 跳出循环后+1
            return (maxLen < (j - i)) ? (j - i) : maxLen;
        }
    }
    return maxLen;
}


#define INPUT_ARR_SIZE 100
int main() {
    // char input[] = "abcabcbb";
    // int out = 0;
    // out = lengthOfLongestSubstring(input);
    // printf("[OUT]: %d\n", out);

    char input[INPUT_ARR_SIZE];
    int caseCont = 0;
    int out = 0;
    while (scanf("%[^\n]%*c", input)!=EOF) {  // 考虑输入空格，所以\n结束
    // while (gets(input)!=NULL) {  // 换行结束，读完后缓存中\n被丢弃， getline()获取一行，换行符结束
    // while (scanf("%s", input)!=EOF) {  // 空格或回车结束, 不适用
        printf("\n[case %d]: \n", caseCont);
        caseCont++;
        out = lengthOfLongestSubstring(input);
        printf("[OUT]: %d\n", out);
    }
    return 0;
}

// scanf("%*[\n]")           // 表示该输入项读入后不赋予任何变量，即scanf("%*[^\n]")表示跳过一行字符串
// scanf("%[^\n]", str1);    // 过程1   [^\n]表示一读入换行字符就结束读入。这个是scanf的正则用法,读完后\n还在缓存中
// scanf("%[^\n]%*c", str)   // 可以将最后的\n吸收掉
// scanf("%[^#]", str2);     // 过程2
// scanf("%*[^\n]", str3);   // 过程3 加了个*
// scanf("%*[^#]", str4);    // 过程4 加了个*

// scanf("%[^\n]%*c", input);
// printf("[input1]:%s\n", input);
// scanf("%[^\n]", input);
// printf("[input2]:%s\n", input);
// scanf("%*[\n]");
// scanf("%[^\n]%*c", input);
// printf("[input3]:%s\n", input);
