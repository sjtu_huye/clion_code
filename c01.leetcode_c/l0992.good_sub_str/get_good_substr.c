/*
 * Created by sjtu_huye on 2021/4/25.
 * Copyright (c) 2021-2021 sjtu_huye@person.com , All rights reserved.
 */

/*
给定一个正整数数组 A，如果 A 的某个子数组中不同整数的个数恰好为 K，则称 A 的这个连续、不一定不同的子数组为好子数组。
（例如，[1,2,3,1,2] 中有 3 个不同的整数：1，2，以及 3。）
返回 A 中好子数组的数目。

示例 1：
输入：A = [1,2,1,2,3], K = 2
输出：7
解释：恰好由 2 个不同整数组成的子数组：[1,2], [2,1], [1,2], [2,3], [1,2,1], [2,1,2], [1,2,1,2].
示例 2：
输入：A = [1,2,1,3,4], K = 3
输出：3
解释：恰好由 3 个不同整数组成的子数组：[1,2,1,3], [2,1,3], [1,3,4].

提示：
1 <= A.length <= 20000
1 <= A[i] <= A.length
1 <= K <= A.length
链接：https://leetcode-cn.com/problems/subarrays-with-k-different-integers
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "stdout_proc.h"

#define MAX_NUM_VALUE 20001

int get_different_digit_num(int* A, int start, int end) {
    int nums[MAX_NUM_VALUE] = {0};
    int cont = 0;
    for (int i = start; i <= end; ++i) {
        if (nums[A[i]] == 0) {
            cont++;
        }
        nums[A[i]]++;
    }
    return cont;
}

int subarraysWithKDistinct0(int* A, int ASize, int K){
    int strNum = 0;
    if (K > ASize) {
        return 0;
    }
    for (int i = 0; i <= ASize - K; ++i) {
        for (int j = i + K - 1; j < ASize; ++j) {
            if(get_different_digit_num(A, i, j) == K) {
                strNum++;
            }
        }
    }
    return strNum;
}

int str_info_backup(int *A, int *nums, int end) {
    for (int i = end; i >= 0; --i) {
        if (nums[A[i]] == 1){
            return i;
        }
        nums[A[i]]--;
    }
    return 0;
}

int subarraysWithKDistinct(int* A, int ASize, int K){
    int strNum = 0;
    int nums[MAX_NUM_VALUE] = {0};
    if (K > ASize) {
        return 0;
    }
    int cont = 0;
    int lastPos = 0;
    for (int i = 0; i <= ASize - K; ++i) {
        if (i != 0) {
            if (nums[A[i - 1]] == 1) {
                cont--;
            } else {
                if (cont == K){
                    strNum++;
                } else{
                    return strNum;
                }
            }
            nums[A[i - 1]]--;
        }
        for (int j = lastPos; j < ASize; ++j) {
            if (nums[A[j]] == 0) {
                if (cont == K) {
                    lastPos = str_info_backup(A,nums,j-1) + 1;
                    break;
                }
                cont++;
            }
            nums[A[j]]++;
            if (cont == K) {
                strNum++;
            }
            if (j == ASize - 1) {
                if(cont < K){
                    return strNum;
                } else {
                    lastPos = str_info_backup(A,nums,j) + 1;  // 由于已经赋值所以也要回退
                }
            }
        }
    }
    return strNum;
}


#define INPUT_ARR_SIZE 100
int main() {
    // int A[] = {1,2,1,2,3};
    // int aSize = sizeof(nums)/sizeof(int);
    // int k = 2;
    // int out = subarraysWithKDistinct(A, aSize, k);
    // printf("[OUT]: %d\n", out);
    char input[INPUT_ARR_SIZE];
    int A[INPUT_ARR_SIZE];
    int k = 0;
    int aSize = 0;
    int out = 0;
    int caseCont = 0;
    while (scanf("%s %d", input, &k)!=EOF) {
        printf("\n[case %d]: \n", caseCont);
        caseCont++;
        aSize = 0;
        OneDimensionalMatrixStrToIntArr(input, A, &aSize);
        printf("[k]: %d\n", k);
        out = subarraysWithKDistinct(A, aSize, k);
        printf("[OUT]: %d\n", out);
    }
    return 0;
}