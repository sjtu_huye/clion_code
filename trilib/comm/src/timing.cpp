#include "timing.h"
#include <sys/time.h>

#define MAX_RECORD_NUM 100
struct timval g_time_record[MAX_RECORD_NUM][2];

void Time_Record_Start(int id)
{
    gettimeofday(&g_time_record[id][0], NULL);
}


void Time_Record_End(int id)
{
    gettimeofday(&g_time_record[id][1], NULL);
}


void Time_Report(void)
{
    LOG_FILE("Time Reporting:\n");
    for (int i = 0; i < MAX_RECORD_NUM - 1; i++)
    {
        struct timeval *start = &g_time_record[i][0];
        struct timeval *end = &g_time_record[i][1];
        long long sec = end->tv_sec - start->tv_sec;
        long long usec = end->tv_usec - start->tv_usec;

        long long cost = sec * 1000 + usec / 1000;
        LOG_FILE("<%d> cost %lld ms\n", i, cost);

    }
}