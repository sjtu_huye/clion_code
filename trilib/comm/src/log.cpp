#include "log.h"
#ifdef _WIN32
const int static MAX_BUFFER_SIZE = 4096*2

TraceLogV::TraceLogV(const std::string &str, const int &line)
: m_fileName(str), m_line(line)
{
    char exeFullPath[MAX_BUFFER_SIZE] = {0};
    GetModuleFileNameA(NULL, exeFullPath, MAX_PATH);
    int pathLen = (int)string(exeFullPath).rfind('.');
    string strFullPath = string(exeFullPath).substr(0, pathLen);
    m_logFile = strFullPath + string("_log.txt");
}

TraceLogV::~TraceLogV() {}

string TraceLogV::ExtractFileName(const std::string &fileFullName)
{
    return string(fileFullName).substr(fileFullName.rfind("\\") + 1, fileFullName.size());
}

string TraceLogV::ExtractFilePath(const std::string &fileFullName)
{
    return string(fileFullName).substr(0, fileFullName.rfind("\\") + 1);
}

string TraceLogV::Format(const char* pszFormat, ...)
{
    va_list args;
    va_start(args, pszFormat);
    char buffer[MAX_BUFFER_SIZE] = {0};
    const int size = vsnprintf_s(buffer, sizeof(buffer) - 1, sizeof(buffer) - 1, pszFormat, args);
    va_end(args);

    if(size <= 0) {
        return "";
    }

    return buffer;
}

string TraceLogV::FormatV(const char* pszFormat, va_list args)
{
    char buffer[MAX_BUFFER_SIZE] = {0};
    const int size = vsnprintf_s(buffer, sizeof(buffer) - 1, sizeof(buffer) - 1, pszFormat, args);

    if(size <= 0) {
        return "";
    }

    return buffer;
}

void TraceLogV::Doit(const char* fmt, ...)
{
    va_list args;
    (void)va_start(args, fmt);
    string msg = FormatV(fmt, args);
    va_end(args);

    string content = Format("%s", msg.c_str());
    static ofstream out(m_logFile.c_str(), ios::out|ios::app);
    out << content;
}

void TraceLogV::Trunc()
{
    ofstream out(m_logFile.c_str(), ios::out|ios::trunc);
}

#else
FILE *g_logFileHandle;
void LogFileInit(int id)
{
    char log_name[100];
    sprintf(log_name, "log_%id.txt", id);
    g_logFileHandle = fopen(log_name, "w+");
    printf("open %s %s.\n", log_name, (g_logFileHandle != NULL ? "succeeded" : "failed"));
}
#endif