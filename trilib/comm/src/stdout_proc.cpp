/*
 * Created by sjtu_huye on 2021/5/02.
 * Copyright (c) 2021-2021 sjtu_huye@person.com , All rights reserved.
 */

#include "stdout_proc.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


void OneDimensionalMatrixStrToIntArr(char *str, int *out, int *outLen) {
    char *token = NULL;
    char key[] = ",";
    int len = 0;
    /* 获取第一个子字符串 */
    token = strtok(&str[1], key);
    /* 继续获取其他的子字符串 */
    printf("[input]: [");

    while( token != NULL ) {
        out[len] = atoi(token);
        printf("%d,", out[len]);
        len++;
        token = strtok(NULL, key);
    }
    printf("\b], [len]: %d\n", len);
    *outLen = len;
}