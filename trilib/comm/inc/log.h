#ifndef __LOG_H__
#define __LOG_H__

#include <stdio.h>
#ifdef _WIN32
#include <fstream>
#include <string>
#include <cstdarg>
#include <windows.h>

using namespace std;
#define TRUNC_LOG TraceLogV(__FILE__, __LINE__).Trunc

#ifdef _DEBUG
#define LOG TraceLogV(__FILE__, __LINE__).Doit
#else
#define LOG printf
#endif


Class TraceLogV
{
public:
    TraceLogV(const std::string &str, const int& line);
    ~TraceLogV();

    void Doit(const char* fmt, ...);
    void Trunc();

private:
    string Format(const char* pszFormat, ...);
    string FormatV(const char* pszFormat, va_list args);
    string ExtrcatFilePath(const std::string &fileFullName);
    string ExtrcatFileName(const std::string &fileFullName);

private:
    TraceLogV(){};
    std::string m_fileName;
    std::string m_logFile;
    int m_line;
};

#else
extern File *g_logFileHandle;
#define LOG_FILE(fmt, ...) fprintf(fp, fmt, ##__VA_ARGS__)
#ifdef _DEBUG
#define LOG(log_level, fmt, ...) LOG_FILE("<%d> %s %d: ", log_level, __FUNCTION__, __LINE__); LOG_FILE(fmt, ##__VA_ARGS__);
#else
#define LOG(log_level, fmt, ...) printf("<%d> %s %d: ", log_level, __FUNCTION__, __LINE__); printf(fmt, ##__VA_ARGS__);
#endif

#define LOG_INFO(fmt, ...) LOG(0, fmt, ##__VA_ARGS__); printf("\n");
void LOGFileInit(int id);
#endif

#endif