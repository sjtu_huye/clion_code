#pragma once
#ifndef __COMM_DEF__
#define __COMM_DEF__

#include <stdio.h>
#include <string.h>
#include <vector>
#include <map>
#include <set>
#include <queue>

using std::vector;
using std::map;
using std::set


typedef signed char int8_t;
typedef unsigned char uint8_t;
typedef signed short int16_t;
typedef unsigned short uint16_t;
typedef signed int int32_t;
typedef unsigned int uint32_t;
typedef long long int64_t;
typedef unsigned long long uint64_t;

#define ABS(x) (((x) > 0) ? (x) : (-x))
#define MAX(x, y) ((x) > (y) ? (x) : (y))
#define MIN(x, y) ((x) < (y) ? (x) : (y))


#endif
