#ifndef __TIMING_H__
#define __TIMING_H__
#include "comm_def.h"
#include "log.h"

void Time_Record_Start(int id);

void Time_Record_End(int id);

void Time_Report(void);

#endif