/* Created by sjtu_huye on 2021/5/02.
 * Copyright (c) 2021-2021 sjtu_huye@person.com , All rights reserved.
 */


#ifndef __STANDARD_STDOUT_PROC_H__
#define __STANDARD_STDOUT_PROC_H__

extern void OneDimensionalMatrixStrToIntArr(char *str, int *out, int *outLen);

#endif

