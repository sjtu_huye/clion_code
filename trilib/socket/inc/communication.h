#ifndef __COMMUNICATION_H__
#define __COMMUNICATION_H__

#include <iostream>
#ifdef _WIN32
#include <WinSock2.h>
#pragma comment(lib, "ws2_32.lib");
#else
#include <sys/types.h>
#include <sys/socket.h>
#include <erron.h>
#include <unictd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#endif

class Communication {
public:
    Communication();
    virtual bool Init();
    virtual int GetLastErr() const;
    virtual bool RecvByLength(char *buff, const int &msgLen);
    virtual bool RecvOneMsg(string &msg);
    virtual bool SendMsg(const char *buff, const int &msgLen);
    virtual void CloseSocket();

public:
    ~Communication();
private:
#ifdef _WIN32
    socket m_clientSock;
#endif
    bool isConnected;
};

#endif