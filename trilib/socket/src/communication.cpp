#include "communication.h"

Communication::Communication()
{
    m_clientSock = 0;
    m_isConnected = false;
}

Communication::~Communication()
{
    CloseSocket();
}

/* 初始化WINSOCK 1.1版本 */
bool Communication::Init()
{
#ifdef _WIN32
    /* 初始化 */
    WORD versionRequested = MAKEWORD(1, 1);
    WSADATA wsadata;
    if(WSAStartup(versionRequested, &wsadata) != 0) {
        printf("[%s]: call WSAStartup failed!", __FUNCTION__);
        return false;
    }

    /* 检查启动版本与设置版本是否一致 */
    if((LOBYTE(wsadata.wVersion) != 1) || (HIBYTE(wsadata.wVersion) != 1)) {
        printf("[%s]: winsock version is inconsistent!", __FUNCTION__);
        WSACleanup();
        return false;
    }
#endif
    return true;
}

int Communication::GetLastErr() const
{
#ifdef _WIN32
    return WSAGetLastError();
#else
    return -1;
#endif //_WIN32
}

void Communicataion::ConnectToServer(const string &serverIp, unsigned short serverPort) {
    if(m_isConnected) {
        return;
    }

    /* 创建socket */
    m_clientSock = socket(AF_INET, SOCKET_STREAM, 0);
    struct sockaddr_in addrSrv;
    addrSrv.sin_addr.s_addr = inet_addr(serverIp.c_str());
    addrSrv.sin_family = AF_INET;
    addrSrv.sin_port = htons(serverPort);

    // 尝试到链接成功为止
    while(connect(m_clientSock, (sockaddr*)&addrSrv), sizeof(sockaddr) != 0) {
#ifdef _WIN32
        sleep(10);
#else
        sleep(1);
#endif //_WIN32
    }

    m_isConnected = true;
}

/* 接收指定长度数据 */
bool Communication::RecvByLength(char *buff, const int &msgLen)
{
    int remainLen = msgLen;
    do {
        int size = recv(m_clientSock, buff, remainLen, 0);
        if (size > 0) {
            buff += size;
            remainLen -= size;
        } else {
            printf("[%s] recv failed(%d), errorno(%d)", __FUNCTION__, size, GetLastErr());
            return false;
        }
    } while(remainLen > 0);
    return true;
}


bool Communication::RecvOneMsg(string &msg) 
{
    char msgLenBuff[MSG_HEAD_LEN + 1] = {0};
    if(!RecvByLength(msgLenBuff, MSG_HEAD_LEN)) {
        return false;
    }

    int msgSize = atoi(msgLenBuff);
    if(msgSize < 0) {
        return false;
    }

    char *msgBuff = new char[msgSize + 1];
    msgBuff[msgSize] = '\0';
    if(!RecvByLength(msgBuff, msgSize)) {
        delete[] msgBuff;
        return false;
    }

    msg = string(msgBuff);
    delete[] msgBuff;
    return true;
}

bool Communication::SendMsg(const char *buff, const int &msgLen)
{
    int code = send(m_clientSock, buff, msgLen, 0);
#ifdef _WIN32
    if(code == SOCKET_ERROR) {
#else
    if(code == -1) {
#endif //_WIN32
        printf("[%s] send msg failed(%d), errorno(%d)", __FUNCTION__, code, GetLastErr());
        return false;
    }
    return true;
}

void Communication::CloseSocket()
{
#ifdef _WIN32
    shutdown(m_clientSock, SD_BOTH);
    closesocket(m_clientSock);
    WSACleanup();
#else
    close(m_clientSock);
#endif //_WIN32
    m_isConnected = false;
}